// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef TOWERDEFENSEC_TowerDefenseCGameMode_generated_h
#error "TowerDefenseCGameMode.generated.h already included, missing '#pragma once' in TowerDefenseCGameMode.h"
#endif
#define TOWERDEFENSEC_TowerDefenseCGameMode_generated_h

#define TowerDefenseC_Source_TowerDefenseC_TowerDefenseCGameMode_h_12_SPARSE_DATA
#define TowerDefenseC_Source_TowerDefenseC_TowerDefenseCGameMode_h_12_RPC_WRAPPERS
#define TowerDefenseC_Source_TowerDefenseC_TowerDefenseCGameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define TowerDefenseC_Source_TowerDefenseC_TowerDefenseCGameMode_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesATowerDefenseCGameMode(); \
	friend struct Z_Construct_UClass_ATowerDefenseCGameMode_Statics; \
public: \
	DECLARE_CLASS(ATowerDefenseCGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/TowerDefenseC"), TOWERDEFENSEC_API) \
	DECLARE_SERIALIZER(ATowerDefenseCGameMode)


#define TowerDefenseC_Source_TowerDefenseC_TowerDefenseCGameMode_h_12_INCLASS \
private: \
	static void StaticRegisterNativesATowerDefenseCGameMode(); \
	friend struct Z_Construct_UClass_ATowerDefenseCGameMode_Statics; \
public: \
	DECLARE_CLASS(ATowerDefenseCGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/TowerDefenseC"), TOWERDEFENSEC_API) \
	DECLARE_SERIALIZER(ATowerDefenseCGameMode)


#define TowerDefenseC_Source_TowerDefenseC_TowerDefenseCGameMode_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	TOWERDEFENSEC_API ATowerDefenseCGameMode(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ATowerDefenseCGameMode) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(TOWERDEFENSEC_API, ATowerDefenseCGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ATowerDefenseCGameMode); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	TOWERDEFENSEC_API ATowerDefenseCGameMode(ATowerDefenseCGameMode&&); \
	TOWERDEFENSEC_API ATowerDefenseCGameMode(const ATowerDefenseCGameMode&); \
public:


#define TowerDefenseC_Source_TowerDefenseC_TowerDefenseCGameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	TOWERDEFENSEC_API ATowerDefenseCGameMode(ATowerDefenseCGameMode&&); \
	TOWERDEFENSEC_API ATowerDefenseCGameMode(const ATowerDefenseCGameMode&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(TOWERDEFENSEC_API, ATowerDefenseCGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ATowerDefenseCGameMode); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ATowerDefenseCGameMode)


#define TowerDefenseC_Source_TowerDefenseC_TowerDefenseCGameMode_h_12_PRIVATE_PROPERTY_OFFSET
#define TowerDefenseC_Source_TowerDefenseC_TowerDefenseCGameMode_h_9_PROLOG
#define TowerDefenseC_Source_TowerDefenseC_TowerDefenseCGameMode_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TowerDefenseC_Source_TowerDefenseC_TowerDefenseCGameMode_h_12_PRIVATE_PROPERTY_OFFSET \
	TowerDefenseC_Source_TowerDefenseC_TowerDefenseCGameMode_h_12_SPARSE_DATA \
	TowerDefenseC_Source_TowerDefenseC_TowerDefenseCGameMode_h_12_RPC_WRAPPERS \
	TowerDefenseC_Source_TowerDefenseC_TowerDefenseCGameMode_h_12_INCLASS \
	TowerDefenseC_Source_TowerDefenseC_TowerDefenseCGameMode_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define TowerDefenseC_Source_TowerDefenseC_TowerDefenseCGameMode_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TowerDefenseC_Source_TowerDefenseC_TowerDefenseCGameMode_h_12_PRIVATE_PROPERTY_OFFSET \
	TowerDefenseC_Source_TowerDefenseC_TowerDefenseCGameMode_h_12_SPARSE_DATA \
	TowerDefenseC_Source_TowerDefenseC_TowerDefenseCGameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	TowerDefenseC_Source_TowerDefenseC_TowerDefenseCGameMode_h_12_INCLASS_NO_PURE_DECLS \
	TowerDefenseC_Source_TowerDefenseC_TowerDefenseCGameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> TOWERDEFENSEC_API UClass* StaticClass<class ATowerDefenseCGameMode>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID TowerDefenseC_Source_TowerDefenseC_TowerDefenseCGameMode_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
