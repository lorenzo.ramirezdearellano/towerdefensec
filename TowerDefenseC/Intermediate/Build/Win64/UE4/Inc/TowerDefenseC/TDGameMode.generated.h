// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UWaveAssest;
class ASpawner;
class AEnemy;
#ifdef TOWERDEFENSEC_TDGameMode_generated_h
#error "TDGameMode.generated.h already included, missing '#pragma once' in TDGameMode.h"
#endif
#define TOWERDEFENSEC_TDGameMode_generated_h

#define TowerDefenseC_Source_TowerDefenseC_TDGameMode_h_23_DELEGATE \
struct _Script_TowerDefenseC_eventwaveClear_Parms \
{ \
	int32 clearReward; \
}; \
static inline void FwaveClear_DelegateWrapper(const FMulticastScriptDelegate& waveClear, int32 clearReward) \
{ \
	_Script_TowerDefenseC_eventwaveClear_Parms Parms; \
	Parms.clearReward=clearReward; \
	waveClear.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define TowerDefenseC_Source_TowerDefenseC_TDGameMode_h_21_DELEGATE \
struct _Script_TowerDefenseC_eventenemyDefeated_Parms \
{ \
	int32 killReward; \
}; \
static inline void FenemyDefeated_DelegateWrapper(const FMulticastScriptDelegate& enemyDefeated, int32 killReward) \
{ \
	_Script_TowerDefenseC_eventenemyDefeated_Parms Parms; \
	Parms.killReward=killReward; \
	enemyDefeated.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define TowerDefenseC_Source_TowerDefenseC_TDGameMode_h_19_DELEGATE \
struct _Script_TowerDefenseC_eventWaveStart_Parms \
{ \
	UWaveAssest* wavedata; \
}; \
static inline void FWaveStart_DelegateWrapper(const FMulticastScriptDelegate& WaveStart, UWaveAssest* wavedata) \
{ \
	_Script_TowerDefenseC_eventWaveStart_Parms Parms; \
	Parms.wavedata=wavedata; \
	WaveStart.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define TowerDefenseC_Source_TowerDefenseC_TDGameMode_h_28_SPARSE_DATA
#define TowerDefenseC_Source_TowerDefenseC_TDGameMode_h_28_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execWaveClear); \
	DECLARE_FUNCTION(execSendingWaves); \
	DECLARE_FUNCTION(execWaveStarted); \
	DECLARE_FUNCTION(execEnemySpawned); \
	DECLARE_FUNCTION(execEnemyDefeated); \
	DECLARE_FUNCTION(execGetDifficultyMultiplier);


#define TowerDefenseC_Source_TowerDefenseC_TDGameMode_h_28_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execWaveClear); \
	DECLARE_FUNCTION(execSendingWaves); \
	DECLARE_FUNCTION(execWaveStarted); \
	DECLARE_FUNCTION(execEnemySpawned); \
	DECLARE_FUNCTION(execEnemyDefeated); \
	DECLARE_FUNCTION(execGetDifficultyMultiplier);


#define TowerDefenseC_Source_TowerDefenseC_TDGameMode_h_28_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesATDGameMode(); \
	friend struct Z_Construct_UClass_ATDGameMode_Statics; \
public: \
	DECLARE_CLASS(ATDGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/TowerDefenseC"), NO_API) \
	DECLARE_SERIALIZER(ATDGameMode)


#define TowerDefenseC_Source_TowerDefenseC_TDGameMode_h_28_INCLASS \
private: \
	static void StaticRegisterNativesATDGameMode(); \
	friend struct Z_Construct_UClass_ATDGameMode_Statics; \
public: \
	DECLARE_CLASS(ATDGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/TowerDefenseC"), NO_API) \
	DECLARE_SERIALIZER(ATDGameMode)


#define TowerDefenseC_Source_TowerDefenseC_TDGameMode_h_28_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ATDGameMode(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ATDGameMode) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ATDGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ATDGameMode); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ATDGameMode(ATDGameMode&&); \
	NO_API ATDGameMode(const ATDGameMode&); \
public:


#define TowerDefenseC_Source_TowerDefenseC_TDGameMode_h_28_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ATDGameMode(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ATDGameMode(ATDGameMode&&); \
	NO_API ATDGameMode(const ATDGameMode&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ATDGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ATDGameMode); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ATDGameMode)


#define TowerDefenseC_Source_TowerDefenseC_TDGameMode_h_28_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__playerHealth() { return STRUCT_OFFSET(ATDGameMode, playerHealth); } \
	FORCEINLINE static uint32 __PPO__NumofEnemiesDestroyed() { return STRUCT_OFFSET(ATDGameMode, NumofEnemiesDestroyed); } \
	FORCEINLINE static uint32 __PPO__currentWave() { return STRUCT_OFFSET(ATDGameMode, currentWave); } \
	FORCEINLINE static uint32 __PPO__WaveInterval() { return STRUCT_OFFSET(ATDGameMode, WaveInterval); } \
	FORCEINLINE static uint32 __PPO__Waves() { return STRUCT_OFFSET(ATDGameMode, Waves); } \
	FORCEINLINE static uint32 __PPO__Spawners() { return STRUCT_OFFSET(ATDGameMode, Spawners); } \
	FORCEINLINE static uint32 __PPO__savedSpawner() { return STRUCT_OFFSET(ATDGameMode, savedSpawner); } \
	FORCEINLINE static uint32 __PPO__player() { return STRUCT_OFFSET(ATDGameMode, player); }


#define TowerDefenseC_Source_TowerDefenseC_TDGameMode_h_25_PROLOG
#define TowerDefenseC_Source_TowerDefenseC_TDGameMode_h_28_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TowerDefenseC_Source_TowerDefenseC_TDGameMode_h_28_PRIVATE_PROPERTY_OFFSET \
	TowerDefenseC_Source_TowerDefenseC_TDGameMode_h_28_SPARSE_DATA \
	TowerDefenseC_Source_TowerDefenseC_TDGameMode_h_28_RPC_WRAPPERS \
	TowerDefenseC_Source_TowerDefenseC_TDGameMode_h_28_INCLASS \
	TowerDefenseC_Source_TowerDefenseC_TDGameMode_h_28_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define TowerDefenseC_Source_TowerDefenseC_TDGameMode_h_28_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TowerDefenseC_Source_TowerDefenseC_TDGameMode_h_28_PRIVATE_PROPERTY_OFFSET \
	TowerDefenseC_Source_TowerDefenseC_TDGameMode_h_28_SPARSE_DATA \
	TowerDefenseC_Source_TowerDefenseC_TDGameMode_h_28_RPC_WRAPPERS_NO_PURE_DECLS \
	TowerDefenseC_Source_TowerDefenseC_TDGameMode_h_28_INCLASS_NO_PURE_DECLS \
	TowerDefenseC_Source_TowerDefenseC_TDGameMode_h_28_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> TOWERDEFENSEC_API UClass* StaticClass<class ATDGameMode>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID TowerDefenseC_Source_TowerDefenseC_TDGameMode_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
