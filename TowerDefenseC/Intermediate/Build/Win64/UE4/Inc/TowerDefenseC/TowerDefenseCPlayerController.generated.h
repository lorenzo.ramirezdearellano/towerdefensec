// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef TOWERDEFENSEC_TowerDefenseCPlayerController_generated_h
#error "TowerDefenseCPlayerController.generated.h already included, missing '#pragma once' in TowerDefenseCPlayerController.h"
#endif
#define TOWERDEFENSEC_TowerDefenseCPlayerController_generated_h

#define TowerDefenseC_Source_TowerDefenseC_TowerDefenseCPlayerController_h_12_SPARSE_DATA
#define TowerDefenseC_Source_TowerDefenseC_TowerDefenseCPlayerController_h_12_RPC_WRAPPERS
#define TowerDefenseC_Source_TowerDefenseC_TowerDefenseCPlayerController_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define TowerDefenseC_Source_TowerDefenseC_TowerDefenseCPlayerController_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesATowerDefenseCPlayerController(); \
	friend struct Z_Construct_UClass_ATowerDefenseCPlayerController_Statics; \
public: \
	DECLARE_CLASS(ATowerDefenseCPlayerController, APlayerController, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/TowerDefenseC"), NO_API) \
	DECLARE_SERIALIZER(ATowerDefenseCPlayerController)


#define TowerDefenseC_Source_TowerDefenseC_TowerDefenseCPlayerController_h_12_INCLASS \
private: \
	static void StaticRegisterNativesATowerDefenseCPlayerController(); \
	friend struct Z_Construct_UClass_ATowerDefenseCPlayerController_Statics; \
public: \
	DECLARE_CLASS(ATowerDefenseCPlayerController, APlayerController, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/TowerDefenseC"), NO_API) \
	DECLARE_SERIALIZER(ATowerDefenseCPlayerController)


#define TowerDefenseC_Source_TowerDefenseC_TowerDefenseCPlayerController_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ATowerDefenseCPlayerController(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ATowerDefenseCPlayerController) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ATowerDefenseCPlayerController); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ATowerDefenseCPlayerController); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ATowerDefenseCPlayerController(ATowerDefenseCPlayerController&&); \
	NO_API ATowerDefenseCPlayerController(const ATowerDefenseCPlayerController&); \
public:


#define TowerDefenseC_Source_TowerDefenseC_TowerDefenseCPlayerController_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ATowerDefenseCPlayerController(ATowerDefenseCPlayerController&&); \
	NO_API ATowerDefenseCPlayerController(const ATowerDefenseCPlayerController&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ATowerDefenseCPlayerController); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ATowerDefenseCPlayerController); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ATowerDefenseCPlayerController)


#define TowerDefenseC_Source_TowerDefenseC_TowerDefenseCPlayerController_h_12_PRIVATE_PROPERTY_OFFSET
#define TowerDefenseC_Source_TowerDefenseC_TowerDefenseCPlayerController_h_9_PROLOG
#define TowerDefenseC_Source_TowerDefenseC_TowerDefenseCPlayerController_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TowerDefenseC_Source_TowerDefenseC_TowerDefenseCPlayerController_h_12_PRIVATE_PROPERTY_OFFSET \
	TowerDefenseC_Source_TowerDefenseC_TowerDefenseCPlayerController_h_12_SPARSE_DATA \
	TowerDefenseC_Source_TowerDefenseC_TowerDefenseCPlayerController_h_12_RPC_WRAPPERS \
	TowerDefenseC_Source_TowerDefenseC_TowerDefenseCPlayerController_h_12_INCLASS \
	TowerDefenseC_Source_TowerDefenseC_TowerDefenseCPlayerController_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define TowerDefenseC_Source_TowerDefenseC_TowerDefenseCPlayerController_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TowerDefenseC_Source_TowerDefenseC_TowerDefenseCPlayerController_h_12_PRIVATE_PROPERTY_OFFSET \
	TowerDefenseC_Source_TowerDefenseC_TowerDefenseCPlayerController_h_12_SPARSE_DATA \
	TowerDefenseC_Source_TowerDefenseC_TowerDefenseCPlayerController_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	TowerDefenseC_Source_TowerDefenseC_TowerDefenseCPlayerController_h_12_INCLASS_NO_PURE_DECLS \
	TowerDefenseC_Source_TowerDefenseC_TowerDefenseCPlayerController_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> TOWERDEFENSEC_API UClass* StaticClass<class ATowerDefenseCPlayerController>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID TowerDefenseC_Source_TowerDefenseC_TowerDefenseCPlayerController_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
