// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "TowerDefenseC/TDGameMode.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeTDGameMode() {}
// Cross Module References
	TOWERDEFENSEC_API UFunction* Z_Construct_UDelegateFunction_TowerDefenseC_waveClear__DelegateSignature();
	UPackage* Z_Construct_UPackage__Script_TowerDefenseC();
	TOWERDEFENSEC_API UFunction* Z_Construct_UDelegateFunction_TowerDefenseC_enemyDefeated__DelegateSignature();
	TOWERDEFENSEC_API UFunction* Z_Construct_UDelegateFunction_TowerDefenseC_WaveStart__DelegateSignature();
	TOWERDEFENSEC_API UClass* Z_Construct_UClass_UWaveAssest_NoRegister();
	TOWERDEFENSEC_API UClass* Z_Construct_UClass_ATDGameMode_NoRegister();
	TOWERDEFENSEC_API UClass* Z_Construct_UClass_ATDGameMode();
	ENGINE_API UClass* Z_Construct_UClass_AGameModeBase();
	TOWERDEFENSEC_API UClass* Z_Construct_UClass_AEnemy_NoRegister();
	TOWERDEFENSEC_API UClass* Z_Construct_UClass_ASpawner_NoRegister();
	TOWERDEFENSEC_API UClass* Z_Construct_UClass_APlayerActor_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
// End Cross Module References
	struct Z_Construct_UDelegateFunction_TowerDefenseC_waveClear__DelegateSignature_Statics
	{
		struct _Script_TowerDefenseC_eventwaveClear_Parms
		{
			int32 clearReward;
		};
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_clearReward;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UDelegateFunction_TowerDefenseC_waveClear__DelegateSignature_Statics::NewProp_clearReward = { "clearReward", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_TowerDefenseC_eventwaveClear_Parms, clearReward), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_TowerDefenseC_waveClear__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_TowerDefenseC_waveClear__DelegateSignature_Statics::NewProp_clearReward,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_TowerDefenseC_waveClear__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "TDGameMode.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_TowerDefenseC_waveClear__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_TowerDefenseC, nullptr, "waveClear__DelegateSignature", nullptr, nullptr, sizeof(_Script_TowerDefenseC_eventwaveClear_Parms), Z_Construct_UDelegateFunction_TowerDefenseC_waveClear__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_TowerDefenseC_waveClear__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_TowerDefenseC_waveClear__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_TowerDefenseC_waveClear__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_TowerDefenseC_waveClear__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_TowerDefenseC_waveClear__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UDelegateFunction_TowerDefenseC_enemyDefeated__DelegateSignature_Statics
	{
		struct _Script_TowerDefenseC_eventenemyDefeated_Parms
		{
			int32 killReward;
		};
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_killReward;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UDelegateFunction_TowerDefenseC_enemyDefeated__DelegateSignature_Statics::NewProp_killReward = { "killReward", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_TowerDefenseC_eventenemyDefeated_Parms, killReward), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_TowerDefenseC_enemyDefeated__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_TowerDefenseC_enemyDefeated__DelegateSignature_Statics::NewProp_killReward,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_TowerDefenseC_enemyDefeated__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "TDGameMode.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_TowerDefenseC_enemyDefeated__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_TowerDefenseC, nullptr, "enemyDefeated__DelegateSignature", nullptr, nullptr, sizeof(_Script_TowerDefenseC_eventenemyDefeated_Parms), Z_Construct_UDelegateFunction_TowerDefenseC_enemyDefeated__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_TowerDefenseC_enemyDefeated__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_TowerDefenseC_enemyDefeated__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_TowerDefenseC_enemyDefeated__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_TowerDefenseC_enemyDefeated__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_TowerDefenseC_enemyDefeated__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UDelegateFunction_TowerDefenseC_WaveStart__DelegateSignature_Statics
	{
		struct _Script_TowerDefenseC_eventWaveStart_Parms
		{
			UWaveAssest* wavedata;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_wavedata;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UDelegateFunction_TowerDefenseC_WaveStart__DelegateSignature_Statics::NewProp_wavedata = { "wavedata", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_TowerDefenseC_eventWaveStart_Parms, wavedata), Z_Construct_UClass_UWaveAssest_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_TowerDefenseC_WaveStart__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_TowerDefenseC_WaveStart__DelegateSignature_Statics::NewProp_wavedata,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_TowerDefenseC_WaveStart__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "ModuleRelativePath", "TDGameMode.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_TowerDefenseC_WaveStart__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_TowerDefenseC, nullptr, "WaveStart__DelegateSignature", nullptr, nullptr, sizeof(_Script_TowerDefenseC_eventWaveStart_Parms), Z_Construct_UDelegateFunction_TowerDefenseC_WaveStart__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_TowerDefenseC_WaveStart__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_TowerDefenseC_WaveStart__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_TowerDefenseC_WaveStart__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_TowerDefenseC_WaveStart__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_TowerDefenseC_WaveStart__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	DEFINE_FUNCTION(ATDGameMode::execWaveClear)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->WaveClear();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ATDGameMode::execSendingWaves)
	{
		P_GET_OBJECT(ASpawner,Z_Param_spawner);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SendingWaves(Z_Param_spawner);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ATDGameMode::execWaveStarted)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->WaveStarted();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ATDGameMode::execEnemySpawned)
	{
		P_GET_OBJECT(AEnemy,Z_Param_enemySpawned);
		P_GET_OBJECT(ASpawner,Z_Param_currentSpawner);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->EnemySpawned(Z_Param_enemySpawned,Z_Param_currentSpawner);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ATDGameMode::execEnemyDefeated)
	{
		P_GET_OBJECT(AEnemy,Z_Param_enemyDefeated);
		P_GET_PROPERTY(FIntProperty,Z_Param_KillReward);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->EnemyDefeated(Z_Param_enemyDefeated,Z_Param_KillReward);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ATDGameMode::execGetDifficultyMultiplier)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(int32*)Z_Param__Result=P_THIS->GetDifficultyMultiplier();
		P_NATIVE_END;
	}
	void ATDGameMode::StaticRegisterNativesATDGameMode()
	{
		UClass* Class = ATDGameMode::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "EnemyDefeated", &ATDGameMode::execEnemyDefeated },
			{ "EnemySpawned", &ATDGameMode::execEnemySpawned },
			{ "GetDifficultyMultiplier", &ATDGameMode::execGetDifficultyMultiplier },
			{ "SendingWaves", &ATDGameMode::execSendingWaves },
			{ "WaveClear", &ATDGameMode::execWaveClear },
			{ "WaveStarted", &ATDGameMode::execWaveStarted },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_ATDGameMode_EnemyDefeated_Statics
	{
		struct TDGameMode_eventEnemyDefeated_Parms
		{
			AEnemy* enemyDefeated;
			int32 KillReward;
		};
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_KillReward;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_enemyDefeated;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_ATDGameMode_EnemyDefeated_Statics::NewProp_KillReward = { "KillReward", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(TDGameMode_eventEnemyDefeated_Parms, KillReward), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ATDGameMode_EnemyDefeated_Statics::NewProp_enemyDefeated = { "enemyDefeated", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(TDGameMode_eventEnemyDefeated_Parms, enemyDefeated), Z_Construct_UClass_AEnemy_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ATDGameMode_EnemyDefeated_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ATDGameMode_EnemyDefeated_Statics::NewProp_KillReward,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ATDGameMode_EnemyDefeated_Statics::NewProp_enemyDefeated,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ATDGameMode_EnemyDefeated_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "TDGameMode.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ATDGameMode_EnemyDefeated_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ATDGameMode, nullptr, "EnemyDefeated", nullptr, nullptr, sizeof(TDGameMode_eventEnemyDefeated_Parms), Z_Construct_UFunction_ATDGameMode_EnemyDefeated_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ATDGameMode_EnemyDefeated_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ATDGameMode_EnemyDefeated_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ATDGameMode_EnemyDefeated_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ATDGameMode_EnemyDefeated()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ATDGameMode_EnemyDefeated_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ATDGameMode_EnemySpawned_Statics
	{
		struct TDGameMode_eventEnemySpawned_Parms
		{
			AEnemy* enemySpawned;
			ASpawner* currentSpawner;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_currentSpawner;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_enemySpawned;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ATDGameMode_EnemySpawned_Statics::NewProp_currentSpawner = { "currentSpawner", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(TDGameMode_eventEnemySpawned_Parms, currentSpawner), Z_Construct_UClass_ASpawner_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ATDGameMode_EnemySpawned_Statics::NewProp_enemySpawned = { "enemySpawned", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(TDGameMode_eventEnemySpawned_Parms, enemySpawned), Z_Construct_UClass_AEnemy_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ATDGameMode_EnemySpawned_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ATDGameMode_EnemySpawned_Statics::NewProp_currentSpawner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ATDGameMode_EnemySpawned_Statics::NewProp_enemySpawned,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ATDGameMode_EnemySpawned_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "TDGameMode.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ATDGameMode_EnemySpawned_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ATDGameMode, nullptr, "EnemySpawned", nullptr, nullptr, sizeof(TDGameMode_eventEnemySpawned_Parms), Z_Construct_UFunction_ATDGameMode_EnemySpawned_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ATDGameMode_EnemySpawned_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00080401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ATDGameMode_EnemySpawned_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ATDGameMode_EnemySpawned_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ATDGameMode_EnemySpawned()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ATDGameMode_EnemySpawned_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ATDGameMode_GetDifficultyMultiplier_Statics
	{
		struct TDGameMode_eventGetDifficultyMultiplier_Parms
		{
			int32 ReturnValue;
		};
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_ATDGameMode_GetDifficultyMultiplier_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(TDGameMode_eventGetDifficultyMultiplier_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ATDGameMode_GetDifficultyMultiplier_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ATDGameMode_GetDifficultyMultiplier_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ATDGameMode_GetDifficultyMultiplier_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "TDGameMode.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ATDGameMode_GetDifficultyMultiplier_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ATDGameMode, nullptr, "GetDifficultyMultiplier", nullptr, nullptr, sizeof(TDGameMode_eventGetDifficultyMultiplier_Parms), Z_Construct_UFunction_ATDGameMode_GetDifficultyMultiplier_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ATDGameMode_GetDifficultyMultiplier_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ATDGameMode_GetDifficultyMultiplier_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ATDGameMode_GetDifficultyMultiplier_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ATDGameMode_GetDifficultyMultiplier()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ATDGameMode_GetDifficultyMultiplier_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ATDGameMode_SendingWaves_Statics
	{
		struct TDGameMode_eventSendingWaves_Parms
		{
			ASpawner* spawner;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_spawner;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ATDGameMode_SendingWaves_Statics::NewProp_spawner = { "spawner", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(TDGameMode_eventSendingWaves_Parms, spawner), Z_Construct_UClass_ASpawner_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ATDGameMode_SendingWaves_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ATDGameMode_SendingWaves_Statics::NewProp_spawner,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ATDGameMode_SendingWaves_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "TDGameMode.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ATDGameMode_SendingWaves_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ATDGameMode, nullptr, "SendingWaves", nullptr, nullptr, sizeof(TDGameMode_eventSendingWaves_Parms), Z_Construct_UFunction_ATDGameMode_SendingWaves_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ATDGameMode_SendingWaves_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00080401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ATDGameMode_SendingWaves_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ATDGameMode_SendingWaves_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ATDGameMode_SendingWaves()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ATDGameMode_SendingWaves_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ATDGameMode_WaveClear_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ATDGameMode_WaveClear_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "TDGameMode.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ATDGameMode_WaveClear_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ATDGameMode, nullptr, "WaveClear", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00080401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ATDGameMode_WaveClear_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ATDGameMode_WaveClear_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ATDGameMode_WaveClear()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ATDGameMode_WaveClear_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ATDGameMode_WaveStarted_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ATDGameMode_WaveStarted_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "TDGameMode.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ATDGameMode_WaveStarted_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ATDGameMode, nullptr, "WaveStarted", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00080401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ATDGameMode_WaveStarted_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ATDGameMode_WaveStarted_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ATDGameMode_WaveStarted()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ATDGameMode_WaveStarted_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_ATDGameMode_NoRegister()
	{
		return ATDGameMode::StaticClass();
	}
	struct Z_Construct_UClass_ATDGameMode_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_player_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_player;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_savedSpawner_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_savedSpawner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Spawners_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Spawners;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Spawners_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Waves_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Waves;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Waves_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WaveInterval_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_WaveInterval;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_currentWave_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_currentWave;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NumofEnemiesDestroyed_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_NumofEnemiesDestroyed;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_playerHealth_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_playerHealth;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ATDGameMode_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AGameModeBase,
		(UObject* (*)())Z_Construct_UPackage__Script_TowerDefenseC,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_ATDGameMode_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_ATDGameMode_EnemyDefeated, "EnemyDefeated" }, // 3984049148
		{ &Z_Construct_UFunction_ATDGameMode_EnemySpawned, "EnemySpawned" }, // 2206071886
		{ &Z_Construct_UFunction_ATDGameMode_GetDifficultyMultiplier, "GetDifficultyMultiplier" }, // 2565584525
		{ &Z_Construct_UFunction_ATDGameMode_SendingWaves, "SendingWaves" }, // 4065965629
		{ &Z_Construct_UFunction_ATDGameMode_WaveClear, "WaveClear" }, // 3233300928
		{ &Z_Construct_UFunction_ATDGameMode_WaveStarted, "WaveStarted" }, // 75931727
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ATDGameMode_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Info Rendering MovementReplication Replication Actor Input Movement Collision Rendering Utilities|Transformation" },
		{ "IncludePath", "TDGameMode.h" },
		{ "ModuleRelativePath", "TDGameMode.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ATDGameMode_Statics::NewProp_player_MetaData[] = {
		{ "Category", "TDGameMode" },
		{ "ModuleRelativePath", "TDGameMode.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ATDGameMode_Statics::NewProp_player = { "player", nullptr, (EPropertyFlags)0x0020080000000005, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ATDGameMode, player), Z_Construct_UClass_APlayerActor_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ATDGameMode_Statics::NewProp_player_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ATDGameMode_Statics::NewProp_player_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ATDGameMode_Statics::NewProp_savedSpawner_MetaData[] = {
		{ "Category", "TDGameMode" },
		{ "ModuleRelativePath", "TDGameMode.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ATDGameMode_Statics::NewProp_savedSpawner = { "savedSpawner", nullptr, (EPropertyFlags)0x0020080000000005, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ATDGameMode, savedSpawner), Z_Construct_UClass_ASpawner_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ATDGameMode_Statics::NewProp_savedSpawner_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ATDGameMode_Statics::NewProp_savedSpawner_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ATDGameMode_Statics::NewProp_Spawners_MetaData[] = {
		{ "Category", "TDGameMode" },
		{ "ModuleRelativePath", "TDGameMode.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_ATDGameMode_Statics::NewProp_Spawners = { "Spawners", nullptr, (EPropertyFlags)0x0020080000000005, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ATDGameMode, Spawners), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_ATDGameMode_Statics::NewProp_Spawners_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ATDGameMode_Statics::NewProp_Spawners_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ATDGameMode_Statics::NewProp_Spawners_Inner = { "Spawners", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ATDGameMode_Statics::NewProp_Waves_MetaData[] = {
		{ "Category", "TDGameMode" },
		{ "ModuleRelativePath", "TDGameMode.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_ATDGameMode_Statics::NewProp_Waves = { "Waves", nullptr, (EPropertyFlags)0x0020080000000005, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ATDGameMode, Waves), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_ATDGameMode_Statics::NewProp_Waves_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ATDGameMode_Statics::NewProp_Waves_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ATDGameMode_Statics::NewProp_Waves_Inner = { "Waves", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UWaveAssest_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ATDGameMode_Statics::NewProp_WaveInterval_MetaData[] = {
		{ "Category", "TDGameMode" },
		{ "ModuleRelativePath", "TDGameMode.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ATDGameMode_Statics::NewProp_WaveInterval = { "WaveInterval", nullptr, (EPropertyFlags)0x0020080000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ATDGameMode, WaveInterval), METADATA_PARAMS(Z_Construct_UClass_ATDGameMode_Statics::NewProp_WaveInterval_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ATDGameMode_Statics::NewProp_WaveInterval_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ATDGameMode_Statics::NewProp_currentWave_MetaData[] = {
		{ "Category", "TDGameMode" },
		{ "ModuleRelativePath", "TDGameMode.h" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_ATDGameMode_Statics::NewProp_currentWave = { "currentWave", nullptr, (EPropertyFlags)0x0020080000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ATDGameMode, currentWave), METADATA_PARAMS(Z_Construct_UClass_ATDGameMode_Statics::NewProp_currentWave_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ATDGameMode_Statics::NewProp_currentWave_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ATDGameMode_Statics::NewProp_NumofEnemiesDestroyed_MetaData[] = {
		{ "Category", "TDGameMode" },
		{ "ModuleRelativePath", "TDGameMode.h" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_ATDGameMode_Statics::NewProp_NumofEnemiesDestroyed = { "NumofEnemiesDestroyed", nullptr, (EPropertyFlags)0x0020080000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ATDGameMode, NumofEnemiesDestroyed), METADATA_PARAMS(Z_Construct_UClass_ATDGameMode_Statics::NewProp_NumofEnemiesDestroyed_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ATDGameMode_Statics::NewProp_NumofEnemiesDestroyed_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ATDGameMode_Statics::NewProp_playerHealth_MetaData[] = {
		{ "Category", "TDGameMode" },
		{ "ModuleRelativePath", "TDGameMode.h" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_ATDGameMode_Statics::NewProp_playerHealth = { "playerHealth", nullptr, (EPropertyFlags)0x0020080000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ATDGameMode, playerHealth), METADATA_PARAMS(Z_Construct_UClass_ATDGameMode_Statics::NewProp_playerHealth_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ATDGameMode_Statics::NewProp_playerHealth_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ATDGameMode_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ATDGameMode_Statics::NewProp_player,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ATDGameMode_Statics::NewProp_savedSpawner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ATDGameMode_Statics::NewProp_Spawners,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ATDGameMode_Statics::NewProp_Spawners_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ATDGameMode_Statics::NewProp_Waves,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ATDGameMode_Statics::NewProp_Waves_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ATDGameMode_Statics::NewProp_WaveInterval,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ATDGameMode_Statics::NewProp_currentWave,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ATDGameMode_Statics::NewProp_NumofEnemiesDestroyed,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ATDGameMode_Statics::NewProp_playerHealth,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ATDGameMode_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ATDGameMode>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ATDGameMode_Statics::ClassParams = {
		&ATDGameMode::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_ATDGameMode_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_ATDGameMode_Statics::PropPointers),
		0,
		0x009002ACu,
		METADATA_PARAMS(Z_Construct_UClass_ATDGameMode_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ATDGameMode_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ATDGameMode()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ATDGameMode_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ATDGameMode, 1989007943);
	template<> TOWERDEFENSEC_API UClass* StaticClass<ATDGameMode>()
	{
		return ATDGameMode::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ATDGameMode(Z_Construct_UClass_ATDGameMode, &ATDGameMode::StaticClass, TEXT("/Script/TowerDefenseC"), TEXT("ATDGameMode"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ATDGameMode);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
