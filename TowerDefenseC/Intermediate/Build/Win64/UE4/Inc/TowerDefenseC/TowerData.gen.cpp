// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "TowerDefenseC/TowerData.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeTowerData() {}
// Cross Module References
	TOWERDEFENSEC_API UScriptStruct* Z_Construct_UScriptStruct_FStatLevels();
	UPackage* Z_Construct_UPackage__Script_TowerDefenseC();
	TOWERDEFENSEC_API UClass* Z_Construct_UClass_UTowerData_NoRegister();
	TOWERDEFENSEC_API UClass* Z_Construct_UClass_UTowerData();
	ENGINE_API UClass* Z_Construct_UClass_UDataAsset();
	ENGINE_API UClass* Z_Construct_UClass_UMaterial_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_USphereComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UStaticMesh_NoRegister();
// End Cross Module References
class UScriptStruct* FStatLevels::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern TOWERDEFENSEC_API uint32 Get_Z_Construct_UScriptStruct_FStatLevels_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FStatLevels, Z_Construct_UPackage__Script_TowerDefenseC(), TEXT("StatLevels"), sizeof(FStatLevels), Get_Z_Construct_UScriptStruct_FStatLevels_Hash());
	}
	return Singleton;
}
template<> TOWERDEFENSEC_API UScriptStruct* StaticStruct<FStatLevels>()
{
	return FStatLevels::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FStatLevels(FStatLevels::StaticStruct, TEXT("/Script/TowerDefenseC"), TEXT("StatLevels"), false, nullptr, nullptr);
static struct FScriptStruct_TowerDefenseC_StaticRegisterNativesFStatLevels
{
	FScriptStruct_TowerDefenseC_StaticRegisterNativesFStatLevels()
	{
		UScriptStruct::DeferCppStructOps(FName(TEXT("StatLevels")),new UScriptStruct::TCppStructOps<FStatLevels>);
	}
} ScriptStruct_TowerDefenseC_StaticRegisterNativesFStatLevels;
	struct Z_Construct_UScriptStruct_FStatLevels_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TowerCost_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_TowerCost;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_TowerCost_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Range_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Range;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_Range_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FireRate_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_FireRate;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_FireRate_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Power_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Power;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_Power_Inner;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FStatLevels_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/**\n * \n */" },
		{ "ModuleRelativePath", "TowerData.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FStatLevels_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FStatLevels>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FStatLevels_Statics::NewProp_TowerCost_MetaData[] = {
		{ "Category", "StatLevels" },
		{ "ModuleRelativePath", "TowerData.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FStatLevels_Statics::NewProp_TowerCost = { "TowerCost", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FStatLevels, TowerCost), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FStatLevels_Statics::NewProp_TowerCost_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FStatLevels_Statics::NewProp_TowerCost_MetaData)) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UScriptStruct_FStatLevels_Statics::NewProp_TowerCost_Inner = { "TowerCost", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FStatLevels_Statics::NewProp_Range_MetaData[] = {
		{ "Category", "StatLevels" },
		{ "ModuleRelativePath", "TowerData.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FStatLevels_Statics::NewProp_Range = { "Range", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FStatLevels, Range), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FStatLevels_Statics::NewProp_Range_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FStatLevels_Statics::NewProp_Range_MetaData)) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UScriptStruct_FStatLevels_Statics::NewProp_Range_Inner = { "Range", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FStatLevels_Statics::NewProp_FireRate_MetaData[] = {
		{ "Category", "StatLevels" },
		{ "ModuleRelativePath", "TowerData.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FStatLevels_Statics::NewProp_FireRate = { "FireRate", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FStatLevels, FireRate), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FStatLevels_Statics::NewProp_FireRate_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FStatLevels_Statics::NewProp_FireRate_MetaData)) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UScriptStruct_FStatLevels_Statics::NewProp_FireRate_Inner = { "FireRate", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FStatLevels_Statics::NewProp_Power_MetaData[] = {
		{ "Category", "StatLevels" },
		{ "ModuleRelativePath", "TowerData.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FStatLevels_Statics::NewProp_Power = { "Power", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FStatLevels, Power), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FStatLevels_Statics::NewProp_Power_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FStatLevels_Statics::NewProp_Power_MetaData)) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UScriptStruct_FStatLevels_Statics::NewProp_Power_Inner = { "Power", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FStatLevels_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FStatLevels_Statics::NewProp_TowerCost,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FStatLevels_Statics::NewProp_TowerCost_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FStatLevels_Statics::NewProp_Range,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FStatLevels_Statics::NewProp_Range_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FStatLevels_Statics::NewProp_FireRate,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FStatLevels_Statics::NewProp_FireRate_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FStatLevels_Statics::NewProp_Power,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FStatLevels_Statics::NewProp_Power_Inner,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FStatLevels_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_TowerDefenseC,
		nullptr,
		&NewStructOps,
		"StatLevels",
		sizeof(FStatLevels),
		alignof(FStatLevels),
		Z_Construct_UScriptStruct_FStatLevels_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FStatLevels_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FStatLevels_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FStatLevels_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FStatLevels()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FStatLevels_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_TowerDefenseC();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("StatLevels"), sizeof(FStatLevels), Get_Z_Construct_UScriptStruct_FStatLevels_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FStatLevels_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FStatLevels_Hash() { return 1762764905U; }
	void UTowerData::StaticRegisterNativesUTowerData()
	{
	}
	UClass* Z_Construct_UClass_UTowerData_NoRegister()
	{
		return UTowerData::StaticClass();
	}
	struct Z_Construct_UClass_UTowerData_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LevelMaterial_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_LevelMaterial;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_LevelMaterial_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ActivationRange_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ActivationRange;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_StaticMesh_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_StaticMesh;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Stats_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Stats;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UTowerData_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDataAsset,
		(UObject* (*)())Z_Construct_UPackage__Script_TowerDefenseC,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTowerData_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IncludePath", "TowerData.h" },
		{ "ModuleRelativePath", "TowerData.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTowerData_Statics::NewProp_LevelMaterial_MetaData[] = {
		{ "Category", "TowerData" },
		{ "ModuleRelativePath", "TowerData.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UTowerData_Statics::NewProp_LevelMaterial = { "LevelMaterial", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UTowerData, LevelMaterial), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UTowerData_Statics::NewProp_LevelMaterial_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTowerData_Statics::NewProp_LevelMaterial_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UTowerData_Statics::NewProp_LevelMaterial_Inner = { "LevelMaterial", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UMaterial_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTowerData_Statics::NewProp_ActivationRange_MetaData[] = {
		{ "Category", "TowerData" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "TowerData.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UTowerData_Statics::NewProp_ActivationRange = { "ActivationRange", nullptr, (EPropertyFlags)0x001000000008000d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UTowerData, ActivationRange), Z_Construct_UClass_USphereComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UTowerData_Statics::NewProp_ActivationRange_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTowerData_Statics::NewProp_ActivationRange_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTowerData_Statics::NewProp_StaticMesh_MetaData[] = {
		{ "Category", "TowerData" },
		{ "ModuleRelativePath", "TowerData.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UTowerData_Statics::NewProp_StaticMesh = { "StaticMesh", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UTowerData, StaticMesh), Z_Construct_UClass_UStaticMesh_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UTowerData_Statics::NewProp_StaticMesh_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTowerData_Statics::NewProp_StaticMesh_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTowerData_Statics::NewProp_Stats_MetaData[] = {
		{ "Category", "TowerStats" },
		{ "ModuleRelativePath", "TowerData.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UTowerData_Statics::NewProp_Stats = { "Stats", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UTowerData, Stats), Z_Construct_UScriptStruct_FStatLevels, METADATA_PARAMS(Z_Construct_UClass_UTowerData_Statics::NewProp_Stats_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTowerData_Statics::NewProp_Stats_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UTowerData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTowerData_Statics::NewProp_LevelMaterial,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTowerData_Statics::NewProp_LevelMaterial_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTowerData_Statics::NewProp_ActivationRange,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTowerData_Statics::NewProp_StaticMesh,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTowerData_Statics::NewProp_Stats,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UTowerData_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UTowerData>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UTowerData_Statics::ClassParams = {
		&UTowerData::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UTowerData_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UTowerData_Statics::PropPointers),
		0,
		0x009000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UTowerData_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UTowerData_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UTowerData()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UTowerData_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UTowerData, 2418354906);
	template<> TOWERDEFENSEC_API UClass* StaticClass<UTowerData>()
	{
		return UTowerData::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UTowerData(Z_Construct_UClass_UTowerData, &UTowerData::StaticClass, TEXT("/Script/TowerDefenseC"), TEXT("UTowerData"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UTowerData);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
