// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class AActor;
class UPrimitiveComponent;
struct FHitResult;
#ifdef TOWERDEFENSEC_Tower_Base_generated_h
#error "Tower_Base.generated.h already included, missing '#pragma once' in Tower_Base.h"
#endif
#define TOWERDEFENSEC_Tower_Base_generated_h

#define TowerDefenseC_Source_TowerDefenseC_Tower_Base_h_16_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FTowerStats_Statics; \
	TOWERDEFENSEC_API static class UScriptStruct* StaticStruct();


template<> TOWERDEFENSEC_API UScriptStruct* StaticStruct<struct FTowerStats>();

#define TowerDefenseC_Source_TowerDefenseC_Tower_Base_h_34_SPARSE_DATA
#define TowerDefenseC_Source_TowerDefenseC_Tower_Base_h_34_RPC_WRAPPERS \
	virtual void ActivateTower_Implementation(); \
 \
	DECLARE_FUNCTION(execSetStats); \
	DECLARE_FUNCTION(execUpgrade); \
	DECLARE_FUNCTION(execRotateToTarget); \
	DECLARE_FUNCTION(execOnOverlapEnd); \
	DECLARE_FUNCTION(execOnOverlapBegin); \
	DECLARE_FUNCTION(execBuffingStats); \
	DECLARE_FUNCTION(execremoveTarget); \
	DECLARE_FUNCTION(execacquireTarget); \
	DECLARE_FUNCTION(execActivateTower);


#define TowerDefenseC_Source_TowerDefenseC_Tower_Base_h_34_RPC_WRAPPERS_NO_PURE_DECLS \
	virtual void ActivateTower_Implementation(); \
 \
	DECLARE_FUNCTION(execSetStats); \
	DECLARE_FUNCTION(execUpgrade); \
	DECLARE_FUNCTION(execRotateToTarget); \
	DECLARE_FUNCTION(execOnOverlapEnd); \
	DECLARE_FUNCTION(execOnOverlapBegin); \
	DECLARE_FUNCTION(execBuffingStats); \
	DECLARE_FUNCTION(execremoveTarget); \
	DECLARE_FUNCTION(execacquireTarget); \
	DECLARE_FUNCTION(execActivateTower);


#define TowerDefenseC_Source_TowerDefenseC_Tower_Base_h_34_EVENT_PARMS
#define TowerDefenseC_Source_TowerDefenseC_Tower_Base_h_34_CALLBACK_WRAPPERS
#define TowerDefenseC_Source_TowerDefenseC_Tower_Base_h_34_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesATower_Base(); \
	friend struct Z_Construct_UClass_ATower_Base_Statics; \
public: \
	DECLARE_CLASS(ATower_Base, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/TowerDefenseC"), NO_API) \
	DECLARE_SERIALIZER(ATower_Base)


#define TowerDefenseC_Source_TowerDefenseC_Tower_Base_h_34_INCLASS \
private: \
	static void StaticRegisterNativesATower_Base(); \
	friend struct Z_Construct_UClass_ATower_Base_Statics; \
public: \
	DECLARE_CLASS(ATower_Base, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/TowerDefenseC"), NO_API) \
	DECLARE_SERIALIZER(ATower_Base)


#define TowerDefenseC_Source_TowerDefenseC_Tower_Base_h_34_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ATower_Base(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ATower_Base) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ATower_Base); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ATower_Base); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ATower_Base(ATower_Base&&); \
	NO_API ATower_Base(const ATower_Base&); \
public:


#define TowerDefenseC_Source_TowerDefenseC_Tower_Base_h_34_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ATower_Base(ATower_Base&&); \
	NO_API ATower_Base(const ATower_Base&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ATower_Base); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ATower_Base); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ATower_Base)


#define TowerDefenseC_Source_TowerDefenseC_Tower_Base_h_34_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__StaticMesh() { return STRUCT_OFFSET(ATower_Base, StaticMesh); } \
	FORCEINLINE static uint32 __PPO__ActivationRange() { return STRUCT_OFFSET(ATower_Base, ActivationRange); } \
	FORCEINLINE static uint32 __PPO__ArrayItr() { return STRUCT_OFFSET(ATower_Base, ArrayItr); } \
	FORCEINLINE static uint32 __PPO__targets() { return STRUCT_OFFSET(ATower_Base, targets); } \
	FORCEINLINE static uint32 __PPO__towerData() { return STRUCT_OFFSET(ATower_Base, towerData); } \
	FORCEINLINE static uint32 __PPO__TowerStats() { return STRUCT_OFFSET(ATower_Base, TowerStats); } \
	FORCEINLINE static uint32 __PPO__WillRotate() { return STRUCT_OFFSET(ATower_Base, WillRotate); }


#define TowerDefenseC_Source_TowerDefenseC_Tower_Base_h_31_PROLOG \
	TowerDefenseC_Source_TowerDefenseC_Tower_Base_h_34_EVENT_PARMS


#define TowerDefenseC_Source_TowerDefenseC_Tower_Base_h_34_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TowerDefenseC_Source_TowerDefenseC_Tower_Base_h_34_PRIVATE_PROPERTY_OFFSET \
	TowerDefenseC_Source_TowerDefenseC_Tower_Base_h_34_SPARSE_DATA \
	TowerDefenseC_Source_TowerDefenseC_Tower_Base_h_34_RPC_WRAPPERS \
	TowerDefenseC_Source_TowerDefenseC_Tower_Base_h_34_CALLBACK_WRAPPERS \
	TowerDefenseC_Source_TowerDefenseC_Tower_Base_h_34_INCLASS \
	TowerDefenseC_Source_TowerDefenseC_Tower_Base_h_34_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define TowerDefenseC_Source_TowerDefenseC_Tower_Base_h_34_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TowerDefenseC_Source_TowerDefenseC_Tower_Base_h_34_PRIVATE_PROPERTY_OFFSET \
	TowerDefenseC_Source_TowerDefenseC_Tower_Base_h_34_SPARSE_DATA \
	TowerDefenseC_Source_TowerDefenseC_Tower_Base_h_34_RPC_WRAPPERS_NO_PURE_DECLS \
	TowerDefenseC_Source_TowerDefenseC_Tower_Base_h_34_CALLBACK_WRAPPERS \
	TowerDefenseC_Source_TowerDefenseC_Tower_Base_h_34_INCLASS_NO_PURE_DECLS \
	TowerDefenseC_Source_TowerDefenseC_Tower_Base_h_34_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> TOWERDEFENSEC_API UClass* StaticClass<class ATower_Base>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID TowerDefenseC_Source_TowerDefenseC_Tower_Base_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
