// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class AActor;
struct FVector;
struct FHitResult;
#ifdef TOWERDEFENSEC_PlayerCore_generated_h
#error "PlayerCore.generated.h already included, missing '#pragma once' in PlayerCore.h"
#endif
#define TOWERDEFENSEC_PlayerCore_generated_h

#define TowerDefenseC_Source_TowerDefenseC_PlayerCore_h_16_SPARSE_DATA
#define TowerDefenseC_Source_TowerDefenseC_PlayerCore_h_16_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execOnHit);


#define TowerDefenseC_Source_TowerDefenseC_PlayerCore_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execOnHit);


#define TowerDefenseC_Source_TowerDefenseC_PlayerCore_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAPlayerCore(); \
	friend struct Z_Construct_UClass_APlayerCore_Statics; \
public: \
	DECLARE_CLASS(APlayerCore, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/TowerDefenseC"), NO_API) \
	DECLARE_SERIALIZER(APlayerCore)


#define TowerDefenseC_Source_TowerDefenseC_PlayerCore_h_16_INCLASS \
private: \
	static void StaticRegisterNativesAPlayerCore(); \
	friend struct Z_Construct_UClass_APlayerCore_Statics; \
public: \
	DECLARE_CLASS(APlayerCore, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/TowerDefenseC"), NO_API) \
	DECLARE_SERIALIZER(APlayerCore)


#define TowerDefenseC_Source_TowerDefenseC_PlayerCore_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API APlayerCore(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APlayerCore) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APlayerCore); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APlayerCore); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APlayerCore(APlayerCore&&); \
	NO_API APlayerCore(const APlayerCore&); \
public:


#define TowerDefenseC_Source_TowerDefenseC_PlayerCore_h_16_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APlayerCore(APlayerCore&&); \
	NO_API APlayerCore(const APlayerCore&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APlayerCore); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APlayerCore); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(APlayerCore)


#define TowerDefenseC_Source_TowerDefenseC_PlayerCore_h_16_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__StaticMesh() { return STRUCT_OFFSET(APlayerCore, StaticMesh); }


#define TowerDefenseC_Source_TowerDefenseC_PlayerCore_h_13_PROLOG
#define TowerDefenseC_Source_TowerDefenseC_PlayerCore_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TowerDefenseC_Source_TowerDefenseC_PlayerCore_h_16_PRIVATE_PROPERTY_OFFSET \
	TowerDefenseC_Source_TowerDefenseC_PlayerCore_h_16_SPARSE_DATA \
	TowerDefenseC_Source_TowerDefenseC_PlayerCore_h_16_RPC_WRAPPERS \
	TowerDefenseC_Source_TowerDefenseC_PlayerCore_h_16_INCLASS \
	TowerDefenseC_Source_TowerDefenseC_PlayerCore_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define TowerDefenseC_Source_TowerDefenseC_PlayerCore_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TowerDefenseC_Source_TowerDefenseC_PlayerCore_h_16_PRIVATE_PROPERTY_OFFSET \
	TowerDefenseC_Source_TowerDefenseC_PlayerCore_h_16_SPARSE_DATA \
	TowerDefenseC_Source_TowerDefenseC_PlayerCore_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	TowerDefenseC_Source_TowerDefenseC_PlayerCore_h_16_INCLASS_NO_PURE_DECLS \
	TowerDefenseC_Source_TowerDefenseC_PlayerCore_h_16_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> TOWERDEFENSEC_API UClass* StaticClass<class APlayerCore>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID TowerDefenseC_Source_TowerDefenseC_PlayerCore_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
