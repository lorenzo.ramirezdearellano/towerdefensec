// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "TowerDefenseC/WaypointMovement.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeWaypointMovement() {}
// Cross Module References
	TOWERDEFENSEC_API UClass* Z_Construct_UClass_AWaypointMovement_NoRegister();
	TOWERDEFENSEC_API UClass* Z_Construct_UClass_AWaypointMovement();
	ENGINE_API UClass* Z_Construct_UClass_AStaticMeshActor();
	UPackage* Z_Construct_UPackage__Script_TowerDefenseC();
// End Cross Module References
	void AWaypointMovement::StaticRegisterNativesAWaypointMovement()
	{
	}
	UClass* Z_Construct_UClass_AWaypointMovement_NoRegister()
	{
		return AWaypointMovement::StaticClass();
	}
	struct Z_Construct_UClass_AWaypointMovement_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_waypointOrder_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_waypointOrder;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AWaypointMovement_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AStaticMeshActor,
		(UObject* (*)())Z_Construct_UPackage__Script_TowerDefenseC,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AWaypointMovement_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "HideCategories", "Input" },
		{ "IncludePath", "WaypointMovement.h" },
		{ "ModuleRelativePath", "WaypointMovement.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AWaypointMovement_Statics::NewProp_waypointOrder_MetaData[] = {
		{ "AllowPrivateAccess", "true" },
		{ "Category", "WaypointMovement" },
		{ "ModuleRelativePath", "WaypointMovement.h" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_AWaypointMovement_Statics::NewProp_waypointOrder = { "waypointOrder", nullptr, (EPropertyFlags)0x0040000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AWaypointMovement, waypointOrder), METADATA_PARAMS(Z_Construct_UClass_AWaypointMovement_Statics::NewProp_waypointOrder_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AWaypointMovement_Statics::NewProp_waypointOrder_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AWaypointMovement_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AWaypointMovement_Statics::NewProp_waypointOrder,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AWaypointMovement_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AWaypointMovement>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AWaypointMovement_Statics::ClassParams = {
		&AWaypointMovement::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_AWaypointMovement_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_AWaypointMovement_Statics::PropPointers),
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_AWaypointMovement_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AWaypointMovement_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AWaypointMovement()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AWaypointMovement_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AWaypointMovement, 3845832407);
	template<> TOWERDEFENSEC_API UClass* StaticClass<AWaypointMovement>()
	{
		return AWaypointMovement::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AWaypointMovement(Z_Construct_UClass_AWaypointMovement, &AWaypointMovement::StaticClass, TEXT("/Script/TowerDefenseC"), TEXT("AWaypointMovement"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AWaypointMovement);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
