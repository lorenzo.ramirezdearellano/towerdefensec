// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef TOWERDEFENSEC_Health_generated_h
#error "Health.generated.h already included, missing '#pragma once' in Health.h"
#endif
#define TOWERDEFENSEC_Health_generated_h

#define TowerDefenseC_Source_TowerDefenseC_Health_h_16_SPARSE_DATA
#define TowerDefenseC_Source_TowerDefenseC_Health_h_16_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execTakeDamage);


#define TowerDefenseC_Source_TowerDefenseC_Health_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execTakeDamage);


#define TowerDefenseC_Source_TowerDefenseC_Health_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUHealth(); \
	friend struct Z_Construct_UClass_UHealth_Statics; \
public: \
	DECLARE_CLASS(UHealth, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/TowerDefenseC"), NO_API) \
	DECLARE_SERIALIZER(UHealth)


#define TowerDefenseC_Source_TowerDefenseC_Health_h_16_INCLASS \
private: \
	static void StaticRegisterNativesUHealth(); \
	friend struct Z_Construct_UClass_UHealth_Statics; \
public: \
	DECLARE_CLASS(UHealth, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/TowerDefenseC"), NO_API) \
	DECLARE_SERIALIZER(UHealth)


#define TowerDefenseC_Source_TowerDefenseC_Health_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UHealth(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UHealth) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UHealth); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UHealth); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UHealth(UHealth&&); \
	NO_API UHealth(const UHealth&); \
public:


#define TowerDefenseC_Source_TowerDefenseC_Health_h_16_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UHealth(UHealth&&); \
	NO_API UHealth(const UHealth&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UHealth); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UHealth); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UHealth)


#define TowerDefenseC_Source_TowerDefenseC_Health_h_16_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__MaxHealth() { return STRUCT_OFFSET(UHealth, MaxHealth); } \
	FORCEINLINE static uint32 __PPO__CurrentHealth() { return STRUCT_OFFSET(UHealth, CurrentHealth); }


#define TowerDefenseC_Source_TowerDefenseC_Health_h_13_PROLOG
#define TowerDefenseC_Source_TowerDefenseC_Health_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TowerDefenseC_Source_TowerDefenseC_Health_h_16_PRIVATE_PROPERTY_OFFSET \
	TowerDefenseC_Source_TowerDefenseC_Health_h_16_SPARSE_DATA \
	TowerDefenseC_Source_TowerDefenseC_Health_h_16_RPC_WRAPPERS \
	TowerDefenseC_Source_TowerDefenseC_Health_h_16_INCLASS \
	TowerDefenseC_Source_TowerDefenseC_Health_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define TowerDefenseC_Source_TowerDefenseC_Health_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TowerDefenseC_Source_TowerDefenseC_Health_h_16_PRIVATE_PROPERTY_OFFSET \
	TowerDefenseC_Source_TowerDefenseC_Health_h_16_SPARSE_DATA \
	TowerDefenseC_Source_TowerDefenseC_Health_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	TowerDefenseC_Source_TowerDefenseC_Health_h_16_INCLASS_NO_PURE_DECLS \
	TowerDefenseC_Source_TowerDefenseC_Health_h_16_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> TOWERDEFENSEC_API UClass* StaticClass<class UHealth>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID TowerDefenseC_Source_TowerDefenseC_Health_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
