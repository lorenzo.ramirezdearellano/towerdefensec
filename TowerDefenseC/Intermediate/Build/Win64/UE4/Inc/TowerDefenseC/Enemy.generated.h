// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class AEnemy;
#ifdef TOWERDEFENSEC_Enemy_generated_h
#error "Enemy.generated.h already included, missing '#pragma once' in Enemy.h"
#endif
#define TOWERDEFENSEC_Enemy_generated_h

#define TowerDefenseC_Source_TowerDefenseC_Enemy_h_12_DELEGATE \
struct _Script_TowerDefenseC_eventdestroyedSignature_Parms \
{ \
	AEnemy* enemy; \
	int32 KillReward; \
}; \
static inline void FdestroyedSignature_DelegateWrapper(const FMulticastScriptDelegate& destroyedSignature, AEnemy* enemy, int32 KillReward) \
{ \
	_Script_TowerDefenseC_eventdestroyedSignature_Parms Parms; \
	Parms.enemy=enemy; \
	Parms.KillReward=KillReward; \
	destroyedSignature.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define TowerDefenseC_Source_TowerDefenseC_Enemy_h_25_SPARSE_DATA
#define TowerDefenseC_Source_TowerDefenseC_Enemy_h_25_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execSetKillReward); \
	DECLARE_FUNCTION(execDie);


#define TowerDefenseC_Source_TowerDefenseC_Enemy_h_25_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execSetKillReward); \
	DECLARE_FUNCTION(execDie);


#define TowerDefenseC_Source_TowerDefenseC_Enemy_h_25_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAEnemy(); \
	friend struct Z_Construct_UClass_AEnemy_Statics; \
public: \
	DECLARE_CLASS(AEnemy, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/TowerDefenseC"), NO_API) \
	DECLARE_SERIALIZER(AEnemy)


#define TowerDefenseC_Source_TowerDefenseC_Enemy_h_25_INCLASS \
private: \
	static void StaticRegisterNativesAEnemy(); \
	friend struct Z_Construct_UClass_AEnemy_Statics; \
public: \
	DECLARE_CLASS(AEnemy, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/TowerDefenseC"), NO_API) \
	DECLARE_SERIALIZER(AEnemy)


#define TowerDefenseC_Source_TowerDefenseC_Enemy_h_25_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AEnemy(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AEnemy) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AEnemy); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AEnemy); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AEnemy(AEnemy&&); \
	NO_API AEnemy(const AEnemy&); \
public:


#define TowerDefenseC_Source_TowerDefenseC_Enemy_h_25_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AEnemy(AEnemy&&); \
	NO_API AEnemy(const AEnemy&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AEnemy); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AEnemy); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AEnemy)


#define TowerDefenseC_Source_TowerDefenseC_Enemy_h_25_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__StaticMesh() { return STRUCT_OFFSET(AEnemy, StaticMesh); } \
	FORCEINLINE static uint32 __PPO__HitBox() { return STRUCT_OFFSET(AEnemy, HitBox); } \
	FORCEINLINE static uint32 __PPO__currentType() { return STRUCT_OFFSET(AEnemy, currentType); } \
	FORCEINLINE static uint32 __PPO__healthComponent() { return STRUCT_OFFSET(AEnemy, healthComponent); } \
	FORCEINLINE static uint32 __PPO__KillReward() { return STRUCT_OFFSET(AEnemy, KillReward); } \
	FORCEINLINE static uint32 __PPO__currentWaypoint() { return STRUCT_OFFSET(AEnemy, currentWaypoint); } \
	FORCEINLINE static uint32 __PPO__Waypoints() { return STRUCT_OFFSET(AEnemy, Waypoints); }


#define TowerDefenseC_Source_TowerDefenseC_Enemy_h_22_PROLOG
#define TowerDefenseC_Source_TowerDefenseC_Enemy_h_25_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TowerDefenseC_Source_TowerDefenseC_Enemy_h_25_PRIVATE_PROPERTY_OFFSET \
	TowerDefenseC_Source_TowerDefenseC_Enemy_h_25_SPARSE_DATA \
	TowerDefenseC_Source_TowerDefenseC_Enemy_h_25_RPC_WRAPPERS \
	TowerDefenseC_Source_TowerDefenseC_Enemy_h_25_INCLASS \
	TowerDefenseC_Source_TowerDefenseC_Enemy_h_25_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define TowerDefenseC_Source_TowerDefenseC_Enemy_h_25_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TowerDefenseC_Source_TowerDefenseC_Enemy_h_25_PRIVATE_PROPERTY_OFFSET \
	TowerDefenseC_Source_TowerDefenseC_Enemy_h_25_SPARSE_DATA \
	TowerDefenseC_Source_TowerDefenseC_Enemy_h_25_RPC_WRAPPERS_NO_PURE_DECLS \
	TowerDefenseC_Source_TowerDefenseC_Enemy_h_25_INCLASS_NO_PURE_DECLS \
	TowerDefenseC_Source_TowerDefenseC_Enemy_h_25_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> TOWERDEFENSEC_API UClass* StaticClass<class AEnemy>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID TowerDefenseC_Source_TowerDefenseC_Enemy_h


#define FOREACH_ENUM_ENEMYTYPE(op) \
	op(EnemyType::GROUND) \
	op(EnemyType::FLYING) \
	op(EnemyType::BOSS) 

enum class EnemyType : uint8;
template<> TOWERDEFENSEC_API UEnum* StaticEnum<EnemyType>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
