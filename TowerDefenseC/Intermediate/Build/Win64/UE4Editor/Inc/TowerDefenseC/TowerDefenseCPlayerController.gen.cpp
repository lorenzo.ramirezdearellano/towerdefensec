// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "TowerDefenseC/TowerDefenseCPlayerController.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeTowerDefenseCPlayerController() {}
// Cross Module References
	TOWERDEFENSEC_API UClass* Z_Construct_UClass_ATowerDefenseCPlayerController_NoRegister();
	TOWERDEFENSEC_API UClass* Z_Construct_UClass_ATowerDefenseCPlayerController();
	ENGINE_API UClass* Z_Construct_UClass_APlayerController();
	UPackage* Z_Construct_UPackage__Script_TowerDefenseC();
// End Cross Module References
	void ATowerDefenseCPlayerController::StaticRegisterNativesATowerDefenseCPlayerController()
	{
	}
	UClass* Z_Construct_UClass_ATowerDefenseCPlayerController_NoRegister()
	{
		return ATowerDefenseCPlayerController::StaticClass();
	}
	struct Z_Construct_UClass_ATowerDefenseCPlayerController_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ATowerDefenseCPlayerController_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_APlayerController,
		(UObject* (*)())Z_Construct_UPackage__Script_TowerDefenseC,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ATowerDefenseCPlayerController_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Collision Rendering Utilities|Transformation" },
		{ "IncludePath", "TowerDefenseCPlayerController.h" },
		{ "ModuleRelativePath", "TowerDefenseCPlayerController.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_ATowerDefenseCPlayerController_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ATowerDefenseCPlayerController>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ATowerDefenseCPlayerController_Statics::ClassParams = {
		&ATowerDefenseCPlayerController::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x008002A4u,
		METADATA_PARAMS(Z_Construct_UClass_ATowerDefenseCPlayerController_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ATowerDefenseCPlayerController_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ATowerDefenseCPlayerController()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ATowerDefenseCPlayerController_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ATowerDefenseCPlayerController, 3782517751);
	template<> TOWERDEFENSEC_API UClass* StaticClass<ATowerDefenseCPlayerController>()
	{
		return ATowerDefenseCPlayerController::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ATowerDefenseCPlayerController(Z_Construct_UClass_ATowerDefenseCPlayerController, &ATowerDefenseCPlayerController::StaticClass, TEXT("/Script/TowerDefenseC"), TEXT("ATowerDefenseCPlayerController"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ATowerDefenseCPlayerController);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
