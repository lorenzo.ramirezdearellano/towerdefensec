// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef TOWERDEFENSEC_TowerDefenseCCharacter_generated_h
#error "TowerDefenseCCharacter.generated.h already included, missing '#pragma once' in TowerDefenseCCharacter.h"
#endif
#define TOWERDEFENSEC_TowerDefenseCCharacter_generated_h

#define TowerDefenseC_Source_TowerDefenseC_TowerDefenseCCharacter_h_12_SPARSE_DATA
#define TowerDefenseC_Source_TowerDefenseC_TowerDefenseCCharacter_h_12_RPC_WRAPPERS
#define TowerDefenseC_Source_TowerDefenseC_TowerDefenseCCharacter_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define TowerDefenseC_Source_TowerDefenseC_TowerDefenseCCharacter_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesATowerDefenseCCharacter(); \
	friend struct Z_Construct_UClass_ATowerDefenseCCharacter_Statics; \
public: \
	DECLARE_CLASS(ATowerDefenseCCharacter, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/TowerDefenseC"), NO_API) \
	DECLARE_SERIALIZER(ATowerDefenseCCharacter)


#define TowerDefenseC_Source_TowerDefenseC_TowerDefenseCCharacter_h_12_INCLASS \
private: \
	static void StaticRegisterNativesATowerDefenseCCharacter(); \
	friend struct Z_Construct_UClass_ATowerDefenseCCharacter_Statics; \
public: \
	DECLARE_CLASS(ATowerDefenseCCharacter, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/TowerDefenseC"), NO_API) \
	DECLARE_SERIALIZER(ATowerDefenseCCharacter)


#define TowerDefenseC_Source_TowerDefenseC_TowerDefenseCCharacter_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ATowerDefenseCCharacter(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ATowerDefenseCCharacter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ATowerDefenseCCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ATowerDefenseCCharacter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ATowerDefenseCCharacter(ATowerDefenseCCharacter&&); \
	NO_API ATowerDefenseCCharacter(const ATowerDefenseCCharacter&); \
public:


#define TowerDefenseC_Source_TowerDefenseC_TowerDefenseCCharacter_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ATowerDefenseCCharacter(ATowerDefenseCCharacter&&); \
	NO_API ATowerDefenseCCharacter(const ATowerDefenseCCharacter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ATowerDefenseCCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ATowerDefenseCCharacter); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ATowerDefenseCCharacter)


#define TowerDefenseC_Source_TowerDefenseC_TowerDefenseCCharacter_h_12_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__TopDownCameraComponent() { return STRUCT_OFFSET(ATowerDefenseCCharacter, TopDownCameraComponent); } \
	FORCEINLINE static uint32 __PPO__CameraBoom() { return STRUCT_OFFSET(ATowerDefenseCCharacter, CameraBoom); } \
	FORCEINLINE static uint32 __PPO__CursorToWorld() { return STRUCT_OFFSET(ATowerDefenseCCharacter, CursorToWorld); }


#define TowerDefenseC_Source_TowerDefenseC_TowerDefenseCCharacter_h_9_PROLOG
#define TowerDefenseC_Source_TowerDefenseC_TowerDefenseCCharacter_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TowerDefenseC_Source_TowerDefenseC_TowerDefenseCCharacter_h_12_PRIVATE_PROPERTY_OFFSET \
	TowerDefenseC_Source_TowerDefenseC_TowerDefenseCCharacter_h_12_SPARSE_DATA \
	TowerDefenseC_Source_TowerDefenseC_TowerDefenseCCharacter_h_12_RPC_WRAPPERS \
	TowerDefenseC_Source_TowerDefenseC_TowerDefenseCCharacter_h_12_INCLASS \
	TowerDefenseC_Source_TowerDefenseC_TowerDefenseCCharacter_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define TowerDefenseC_Source_TowerDefenseC_TowerDefenseCCharacter_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TowerDefenseC_Source_TowerDefenseC_TowerDefenseCCharacter_h_12_PRIVATE_PROPERTY_OFFSET \
	TowerDefenseC_Source_TowerDefenseC_TowerDefenseCCharacter_h_12_SPARSE_DATA \
	TowerDefenseC_Source_TowerDefenseC_TowerDefenseCCharacter_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	TowerDefenseC_Source_TowerDefenseC_TowerDefenseCCharacter_h_12_INCLASS_NO_PURE_DECLS \
	TowerDefenseC_Source_TowerDefenseC_TowerDefenseCCharacter_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> TOWERDEFENSEC_API UClass* StaticClass<class ATowerDefenseCCharacter>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID TowerDefenseC_Source_TowerDefenseC_TowerDefenseCCharacter_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
