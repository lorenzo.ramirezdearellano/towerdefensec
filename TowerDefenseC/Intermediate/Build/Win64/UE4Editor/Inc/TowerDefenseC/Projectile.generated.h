// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class AEnemy;
class UPrimitiveComponent;
class AActor;
struct FHitResult;
#ifdef TOWERDEFENSEC_Projectile_generated_h
#error "Projectile.generated.h already included, missing '#pragma once' in Projectile.h"
#endif
#define TOWERDEFENSEC_Projectile_generated_h

#define TowerDefenseC_Source_TowerDefenseC_Projectile_h_15_SPARSE_DATA
#define TowerDefenseC_Source_TowerDefenseC_Projectile_h_15_RPC_WRAPPERS \
	virtual void DamageEnemy_Implementation(AEnemy* enemyHit); \
 \
	DECLARE_FUNCTION(execDamageEnemy); \
	DECLARE_FUNCTION(execsetDamage); \
	DECLARE_FUNCTION(execOnOverlap);


#define TowerDefenseC_Source_TowerDefenseC_Projectile_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	virtual void DamageEnemy_Implementation(AEnemy* enemyHit); \
 \
	DECLARE_FUNCTION(execDamageEnemy); \
	DECLARE_FUNCTION(execsetDamage); \
	DECLARE_FUNCTION(execOnOverlap);


#define TowerDefenseC_Source_TowerDefenseC_Projectile_h_15_EVENT_PARMS \
	struct Projectile_eventDamageEnemy_Parms \
	{ \
		AEnemy* enemyHit; \
	};


#define TowerDefenseC_Source_TowerDefenseC_Projectile_h_15_CALLBACK_WRAPPERS
#define TowerDefenseC_Source_TowerDefenseC_Projectile_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAProjectile(); \
	friend struct Z_Construct_UClass_AProjectile_Statics; \
public: \
	DECLARE_CLASS(AProjectile, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/TowerDefenseC"), NO_API) \
	DECLARE_SERIALIZER(AProjectile)


#define TowerDefenseC_Source_TowerDefenseC_Projectile_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAProjectile(); \
	friend struct Z_Construct_UClass_AProjectile_Statics; \
public: \
	DECLARE_CLASS(AProjectile, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/TowerDefenseC"), NO_API) \
	DECLARE_SERIALIZER(AProjectile)


#define TowerDefenseC_Source_TowerDefenseC_Projectile_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AProjectile(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AProjectile) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AProjectile); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AProjectile); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AProjectile(AProjectile&&); \
	NO_API AProjectile(const AProjectile&); \
public:


#define TowerDefenseC_Source_TowerDefenseC_Projectile_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AProjectile(AProjectile&&); \
	NO_API AProjectile(const AProjectile&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AProjectile); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AProjectile); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AProjectile)


#define TowerDefenseC_Source_TowerDefenseC_Projectile_h_15_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__HitBox() { return STRUCT_OFFSET(AProjectile, HitBox); } \
	FORCEINLINE static uint32 __PPO__StaticMesh() { return STRUCT_OFFSET(AProjectile, StaticMesh); } \
	FORCEINLINE static uint32 __PPO__projectileMovement() { return STRUCT_OFFSET(AProjectile, projectileMovement); } \
	FORCEINLINE static uint32 __PPO__damage() { return STRUCT_OFFSET(AProjectile, damage); }


#define TowerDefenseC_Source_TowerDefenseC_Projectile_h_12_PROLOG \
	TowerDefenseC_Source_TowerDefenseC_Projectile_h_15_EVENT_PARMS


#define TowerDefenseC_Source_TowerDefenseC_Projectile_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TowerDefenseC_Source_TowerDefenseC_Projectile_h_15_PRIVATE_PROPERTY_OFFSET \
	TowerDefenseC_Source_TowerDefenseC_Projectile_h_15_SPARSE_DATA \
	TowerDefenseC_Source_TowerDefenseC_Projectile_h_15_RPC_WRAPPERS \
	TowerDefenseC_Source_TowerDefenseC_Projectile_h_15_CALLBACK_WRAPPERS \
	TowerDefenseC_Source_TowerDefenseC_Projectile_h_15_INCLASS \
	TowerDefenseC_Source_TowerDefenseC_Projectile_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define TowerDefenseC_Source_TowerDefenseC_Projectile_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TowerDefenseC_Source_TowerDefenseC_Projectile_h_15_PRIVATE_PROPERTY_OFFSET \
	TowerDefenseC_Source_TowerDefenseC_Projectile_h_15_SPARSE_DATA \
	TowerDefenseC_Source_TowerDefenseC_Projectile_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	TowerDefenseC_Source_TowerDefenseC_Projectile_h_15_CALLBACK_WRAPPERS \
	TowerDefenseC_Source_TowerDefenseC_Projectile_h_15_INCLASS_NO_PURE_DECLS \
	TowerDefenseC_Source_TowerDefenseC_Projectile_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> TOWERDEFENSEC_API UClass* StaticClass<class AProjectile>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID TowerDefenseC_Source_TowerDefenseC_Projectile_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
