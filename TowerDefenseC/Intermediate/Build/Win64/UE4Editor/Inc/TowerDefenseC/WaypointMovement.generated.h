// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef TOWERDEFENSEC_WaypointMovement_generated_h
#error "WaypointMovement.generated.h already included, missing '#pragma once' in WaypointMovement.h"
#endif
#define TOWERDEFENSEC_WaypointMovement_generated_h

#define TowerDefenseC_Source_TowerDefenseC_WaypointMovement_h_15_SPARSE_DATA
#define TowerDefenseC_Source_TowerDefenseC_WaypointMovement_h_15_RPC_WRAPPERS
#define TowerDefenseC_Source_TowerDefenseC_WaypointMovement_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define TowerDefenseC_Source_TowerDefenseC_WaypointMovement_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAWaypointMovement(); \
	friend struct Z_Construct_UClass_AWaypointMovement_Statics; \
public: \
	DECLARE_CLASS(AWaypointMovement, AStaticMeshActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/TowerDefenseC"), NO_API) \
	DECLARE_SERIALIZER(AWaypointMovement)


#define TowerDefenseC_Source_TowerDefenseC_WaypointMovement_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAWaypointMovement(); \
	friend struct Z_Construct_UClass_AWaypointMovement_Statics; \
public: \
	DECLARE_CLASS(AWaypointMovement, AStaticMeshActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/TowerDefenseC"), NO_API) \
	DECLARE_SERIALIZER(AWaypointMovement)


#define TowerDefenseC_Source_TowerDefenseC_WaypointMovement_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AWaypointMovement(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AWaypointMovement) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AWaypointMovement); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AWaypointMovement); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AWaypointMovement(AWaypointMovement&&); \
	NO_API AWaypointMovement(const AWaypointMovement&); \
public:


#define TowerDefenseC_Source_TowerDefenseC_WaypointMovement_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AWaypointMovement(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AWaypointMovement(AWaypointMovement&&); \
	NO_API AWaypointMovement(const AWaypointMovement&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AWaypointMovement); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AWaypointMovement); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AWaypointMovement)


#define TowerDefenseC_Source_TowerDefenseC_WaypointMovement_h_15_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__waypointOrder() { return STRUCT_OFFSET(AWaypointMovement, waypointOrder); }


#define TowerDefenseC_Source_TowerDefenseC_WaypointMovement_h_12_PROLOG
#define TowerDefenseC_Source_TowerDefenseC_WaypointMovement_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TowerDefenseC_Source_TowerDefenseC_WaypointMovement_h_15_PRIVATE_PROPERTY_OFFSET \
	TowerDefenseC_Source_TowerDefenseC_WaypointMovement_h_15_SPARSE_DATA \
	TowerDefenseC_Source_TowerDefenseC_WaypointMovement_h_15_RPC_WRAPPERS \
	TowerDefenseC_Source_TowerDefenseC_WaypointMovement_h_15_INCLASS \
	TowerDefenseC_Source_TowerDefenseC_WaypointMovement_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define TowerDefenseC_Source_TowerDefenseC_WaypointMovement_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TowerDefenseC_Source_TowerDefenseC_WaypointMovement_h_15_PRIVATE_PROPERTY_OFFSET \
	TowerDefenseC_Source_TowerDefenseC_WaypointMovement_h_15_SPARSE_DATA \
	TowerDefenseC_Source_TowerDefenseC_WaypointMovement_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	TowerDefenseC_Source_TowerDefenseC_WaypointMovement_h_15_INCLASS_NO_PURE_DECLS \
	TowerDefenseC_Source_TowerDefenseC_WaypointMovement_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> TOWERDEFENSEC_API UClass* StaticClass<class AWaypointMovement>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID TowerDefenseC_Source_TowerDefenseC_WaypointMovement_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
