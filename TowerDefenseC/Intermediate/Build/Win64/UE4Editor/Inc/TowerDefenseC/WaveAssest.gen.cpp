// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "TowerDefenseC/WaveAssest.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeWaveAssest() {}
// Cross Module References
	TOWERDEFENSEC_API UScriptStruct* Z_Construct_UScriptStruct_FSpawnDetails();
	UPackage* Z_Construct_UPackage__Script_TowerDefenseC();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	TOWERDEFENSEC_API UClass* Z_Construct_UClass_AEnemy_NoRegister();
	TOWERDEFENSEC_API UClass* Z_Construct_UClass_UWaveAssest_NoRegister();
	TOWERDEFENSEC_API UClass* Z_Construct_UClass_UWaveAssest();
	ENGINE_API UClass* Z_Construct_UClass_UDataAsset();
// End Cross Module References
class UScriptStruct* FSpawnDetails::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern TOWERDEFENSEC_API uint32 Get_Z_Construct_UScriptStruct_FSpawnDetails_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FSpawnDetails, Z_Construct_UPackage__Script_TowerDefenseC(), TEXT("SpawnDetails"), sizeof(FSpawnDetails), Get_Z_Construct_UScriptStruct_FSpawnDetails_Hash());
	}
	return Singleton;
}
template<> TOWERDEFENSEC_API UScriptStruct* StaticStruct<FSpawnDetails>()
{
	return FSpawnDetails::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FSpawnDetails(FSpawnDetails::StaticStruct, TEXT("/Script/TowerDefenseC"), TEXT("SpawnDetails"), false, nullptr, nullptr);
static struct FScriptStruct_TowerDefenseC_StaticRegisterNativesFSpawnDetails
{
	FScriptStruct_TowerDefenseC_StaticRegisterNativesFSpawnDetails()
	{
		UScriptStruct::DeferCppStructOps(FName(TEXT("SpawnDetails")),new UScriptStruct::TCppStructOps<FSpawnDetails>);
	}
} ScriptStruct_TowerDefenseC_StaticRegisterNativesFSpawnDetails;
	struct Z_Construct_UScriptStruct_FSpawnDetails_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SpawnDelay_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_SpawnDelay;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SpawnPointIndex_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_SpawnPointIndex;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EnemyType_MetaData[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_EnemyType;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSpawnDetails_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/**\n * \n */" },
		{ "ModuleRelativePath", "WaveAssest.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FSpawnDetails_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FSpawnDetails>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSpawnDetails_Statics::NewProp_SpawnDelay_MetaData[] = {
		{ "Category", "SpawnDetails" },
		{ "ModuleRelativePath", "WaveAssest.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FSpawnDetails_Statics::NewProp_SpawnDelay = { "SpawnDelay", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSpawnDetails, SpawnDelay), METADATA_PARAMS(Z_Construct_UScriptStruct_FSpawnDetails_Statics::NewProp_SpawnDelay_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSpawnDetails_Statics::NewProp_SpawnDelay_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSpawnDetails_Statics::NewProp_SpawnPointIndex_MetaData[] = {
		{ "Category", "SpawnDetails" },
		{ "ModuleRelativePath", "WaveAssest.h" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UScriptStruct_FSpawnDetails_Statics::NewProp_SpawnPointIndex = { "SpawnPointIndex", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSpawnDetails, SpawnPointIndex), METADATA_PARAMS(Z_Construct_UScriptStruct_FSpawnDetails_Statics::NewProp_SpawnPointIndex_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSpawnDetails_Statics::NewProp_SpawnPointIndex_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSpawnDetails_Statics::NewProp_EnemyType_MetaData[] = {
		{ "Category", "SpawnDetails" },
		{ "ModuleRelativePath", "WaveAssest.h" },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UScriptStruct_FSpawnDetails_Statics::NewProp_EnemyType = { "EnemyType", nullptr, (EPropertyFlags)0x0014000000000015, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSpawnDetails, EnemyType), Z_Construct_UClass_AEnemy_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UScriptStruct_FSpawnDetails_Statics::NewProp_EnemyType_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSpawnDetails_Statics::NewProp_EnemyType_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FSpawnDetails_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSpawnDetails_Statics::NewProp_SpawnDelay,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSpawnDetails_Statics::NewProp_SpawnPointIndex,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSpawnDetails_Statics::NewProp_EnemyType,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FSpawnDetails_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_TowerDefenseC,
		nullptr,
		&NewStructOps,
		"SpawnDetails",
		sizeof(FSpawnDetails),
		alignof(FSpawnDetails),
		Z_Construct_UScriptStruct_FSpawnDetails_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSpawnDetails_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FSpawnDetails_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSpawnDetails_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FSpawnDetails()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FSpawnDetails_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_TowerDefenseC();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("SpawnDetails"), sizeof(FSpawnDetails), Get_Z_Construct_UScriptStruct_FSpawnDetails_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FSpawnDetails_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FSpawnDetails_Hash() { return 1764791445U; }
	void UWaveAssest::StaticRegisterNativesUWaveAssest()
	{
	}
	UClass* Z_Construct_UClass_UWaveAssest_NoRegister()
	{
		return UWaveAssest::StaticClass();
	}
	struct Z_Construct_UClass_UWaveAssest_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WaveReward_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_WaveReward;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_KillReward_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_KillReward;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DifficultyMultiplier_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_DifficultyMultiplier;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EnemySpawnConfig_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_EnemySpawnConfig;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_EnemySpawnConfig_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Boss_MetaData[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_Boss;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EnemyTypes_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_EnemyTypes;
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_EnemyTypes_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NumOfSpawns_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_NumOfSpawns;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WaveDuration_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_WaveDuration;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UWaveAssest_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDataAsset,
		(UObject* (*)())Z_Construct_UPackage__Script_TowerDefenseC,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UWaveAssest_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IncludePath", "WaveAssest.h" },
		{ "ModuleRelativePath", "WaveAssest.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UWaveAssest_Statics::NewProp_WaveReward_MetaData[] = {
		{ "Category", "Reward" },
		{ "ModuleRelativePath", "WaveAssest.h" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_UWaveAssest_Statics::NewProp_WaveReward = { "WaveReward", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UWaveAssest, WaveReward), METADATA_PARAMS(Z_Construct_UClass_UWaveAssest_Statics::NewProp_WaveReward_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UWaveAssest_Statics::NewProp_WaveReward_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UWaveAssest_Statics::NewProp_KillReward_MetaData[] = {
		{ "Category", "Reward" },
		{ "ModuleRelativePath", "WaveAssest.h" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_UWaveAssest_Statics::NewProp_KillReward = { "KillReward", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UWaveAssest, KillReward), METADATA_PARAMS(Z_Construct_UClass_UWaveAssest_Statics::NewProp_KillReward_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UWaveAssest_Statics::NewProp_KillReward_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UWaveAssest_Statics::NewProp_DifficultyMultiplier_MetaData[] = {
		{ "Category", "SpawnDetails" },
		{ "ModuleRelativePath", "WaveAssest.h" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_UWaveAssest_Statics::NewProp_DifficultyMultiplier = { "DifficultyMultiplier", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UWaveAssest, DifficultyMultiplier), METADATA_PARAMS(Z_Construct_UClass_UWaveAssest_Statics::NewProp_DifficultyMultiplier_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UWaveAssest_Statics::NewProp_DifficultyMultiplier_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UWaveAssest_Statics::NewProp_EnemySpawnConfig_MetaData[] = {
		{ "Category", "SpawnDetails" },
		{ "ModuleRelativePath", "WaveAssest.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UWaveAssest_Statics::NewProp_EnemySpawnConfig = { "EnemySpawnConfig", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UWaveAssest, EnemySpawnConfig), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UWaveAssest_Statics::NewProp_EnemySpawnConfig_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UWaveAssest_Statics::NewProp_EnemySpawnConfig_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UWaveAssest_Statics::NewProp_EnemySpawnConfig_Inner = { "EnemySpawnConfig", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FSpawnDetails, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UWaveAssest_Statics::NewProp_Boss_MetaData[] = {
		{ "Category", "Wave Details" },
		{ "ModuleRelativePath", "WaveAssest.h" },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UClass_UWaveAssest_Statics::NewProp_Boss = { "Boss", nullptr, (EPropertyFlags)0x0014000000000015, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UWaveAssest, Boss), Z_Construct_UClass_AEnemy_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UClass_UWaveAssest_Statics::NewProp_Boss_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UWaveAssest_Statics::NewProp_Boss_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UWaveAssest_Statics::NewProp_EnemyTypes_MetaData[] = {
		{ "Category", "Wave Details" },
		{ "ModuleRelativePath", "WaveAssest.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UWaveAssest_Statics::NewProp_EnemyTypes = { "EnemyTypes", nullptr, (EPropertyFlags)0x0014000000000015, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UWaveAssest, EnemyTypes), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UWaveAssest_Statics::NewProp_EnemyTypes_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UWaveAssest_Statics::NewProp_EnemyTypes_MetaData)) };
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UClass_UWaveAssest_Statics::NewProp_EnemyTypes_Inner = { "EnemyTypes", nullptr, (EPropertyFlags)0x0004000000000000, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_AEnemy_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UWaveAssest_Statics::NewProp_NumOfSpawns_MetaData[] = {
		{ "Category", "Wave Details" },
		{ "ModuleRelativePath", "WaveAssest.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UWaveAssest_Statics::NewProp_NumOfSpawns = { "NumOfSpawns", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UWaveAssest, NumOfSpawns), METADATA_PARAMS(Z_Construct_UClass_UWaveAssest_Statics::NewProp_NumOfSpawns_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UWaveAssest_Statics::NewProp_NumOfSpawns_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UWaveAssest_Statics::NewProp_WaveDuration_MetaData[] = {
		{ "Category", "Wave Details" },
		{ "ModuleRelativePath", "WaveAssest.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UWaveAssest_Statics::NewProp_WaveDuration = { "WaveDuration", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UWaveAssest, WaveDuration), METADATA_PARAMS(Z_Construct_UClass_UWaveAssest_Statics::NewProp_WaveDuration_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UWaveAssest_Statics::NewProp_WaveDuration_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UWaveAssest_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UWaveAssest_Statics::NewProp_WaveReward,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UWaveAssest_Statics::NewProp_KillReward,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UWaveAssest_Statics::NewProp_DifficultyMultiplier,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UWaveAssest_Statics::NewProp_EnemySpawnConfig,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UWaveAssest_Statics::NewProp_EnemySpawnConfig_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UWaveAssest_Statics::NewProp_Boss,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UWaveAssest_Statics::NewProp_EnemyTypes,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UWaveAssest_Statics::NewProp_EnemyTypes_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UWaveAssest_Statics::NewProp_NumOfSpawns,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UWaveAssest_Statics::NewProp_WaveDuration,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UWaveAssest_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UWaveAssest>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UWaveAssest_Statics::ClassParams = {
		&UWaveAssest::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UWaveAssest_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UWaveAssest_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UWaveAssest_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UWaveAssest_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UWaveAssest()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UWaveAssest_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UWaveAssest, 3892921493);
	template<> TOWERDEFENSEC_API UClass* StaticClass<UWaveAssest>()
	{
		return UWaveAssest::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UWaveAssest(Z_Construct_UClass_UWaveAssest, &UWaveAssest::StaticClass, TEXT("/Script/TowerDefenseC"), TEXT("UWaveAssest"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UWaveAssest);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
