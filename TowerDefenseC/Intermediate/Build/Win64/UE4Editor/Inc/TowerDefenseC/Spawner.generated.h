// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class AEnemy;
class ASpawner;
#ifdef TOWERDEFENSEC_Spawner_generated_h
#error "Spawner.generated.h already included, missing '#pragma once' in Spawner.h"
#endif
#define TOWERDEFENSEC_Spawner_generated_h

#define TowerDefenseC_Source_TowerDefenseC_Spawner_h_13_DELEGATE \
struct _Script_TowerDefenseC_eventspawned_Parms \
{ \
	AEnemy* enemy; \
	ASpawner* spawnLocation; \
}; \
static inline void Fspawned_DelegateWrapper(const FMulticastScriptDelegate& spawned, AEnemy* enemy, ASpawner* spawnLocation) \
{ \
	_Script_TowerDefenseC_eventspawned_Parms Parms; \
	Parms.enemy=enemy; \
	Parms.spawnLocation=spawnLocation; \
	spawned.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define TowerDefenseC_Source_TowerDefenseC_Spawner_h_18_SPARSE_DATA
#define TowerDefenseC_Source_TowerDefenseC_Spawner_h_18_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execSpawnEnemy);


#define TowerDefenseC_Source_TowerDefenseC_Spawner_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execSpawnEnemy);


#define TowerDefenseC_Source_TowerDefenseC_Spawner_h_18_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesASpawner(); \
	friend struct Z_Construct_UClass_ASpawner_Statics; \
public: \
	DECLARE_CLASS(ASpawner, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/TowerDefenseC"), NO_API) \
	DECLARE_SERIALIZER(ASpawner)


#define TowerDefenseC_Source_TowerDefenseC_Spawner_h_18_INCLASS \
private: \
	static void StaticRegisterNativesASpawner(); \
	friend struct Z_Construct_UClass_ASpawner_Statics; \
public: \
	DECLARE_CLASS(ASpawner, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/TowerDefenseC"), NO_API) \
	DECLARE_SERIALIZER(ASpawner)


#define TowerDefenseC_Source_TowerDefenseC_Spawner_h_18_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASpawner(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASpawner) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASpawner); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASpawner); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASpawner(ASpawner&&); \
	NO_API ASpawner(const ASpawner&); \
public:


#define TowerDefenseC_Source_TowerDefenseC_Spawner_h_18_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASpawner(ASpawner&&); \
	NO_API ASpawner(const ASpawner&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASpawner); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASpawner); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ASpawner)


#define TowerDefenseC_Source_TowerDefenseC_Spawner_h_18_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__SpawnArea() { return STRUCT_OFFSET(ASpawner, SpawnArea); } \
	FORCEINLINE static uint32 __PPO__SpawnerIndex() { return STRUCT_OFFSET(ASpawner, SpawnerIndex); } \
	FORCEINLINE static uint32 __PPO__Wave() { return STRUCT_OFFSET(ASpawner, Wave); }


#define TowerDefenseC_Source_TowerDefenseC_Spawner_h_15_PROLOG
#define TowerDefenseC_Source_TowerDefenseC_Spawner_h_18_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TowerDefenseC_Source_TowerDefenseC_Spawner_h_18_PRIVATE_PROPERTY_OFFSET \
	TowerDefenseC_Source_TowerDefenseC_Spawner_h_18_SPARSE_DATA \
	TowerDefenseC_Source_TowerDefenseC_Spawner_h_18_RPC_WRAPPERS \
	TowerDefenseC_Source_TowerDefenseC_Spawner_h_18_INCLASS \
	TowerDefenseC_Source_TowerDefenseC_Spawner_h_18_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define TowerDefenseC_Source_TowerDefenseC_Spawner_h_18_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TowerDefenseC_Source_TowerDefenseC_Spawner_h_18_PRIVATE_PROPERTY_OFFSET \
	TowerDefenseC_Source_TowerDefenseC_Spawner_h_18_SPARSE_DATA \
	TowerDefenseC_Source_TowerDefenseC_Spawner_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
	TowerDefenseC_Source_TowerDefenseC_Spawner_h_18_INCLASS_NO_PURE_DECLS \
	TowerDefenseC_Source_TowerDefenseC_Spawner_h_18_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> TOWERDEFENSEC_API UClass* StaticClass<class ASpawner>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID TowerDefenseC_Source_TowerDefenseC_Spawner_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
