// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class AActor;
#ifdef TOWERDEFENSEC_Buff_Base_generated_h
#error "Buff_Base.generated.h already included, missing '#pragma once' in Buff_Base.h"
#endif
#define TOWERDEFENSEC_Buff_Base_generated_h

#define TowerDefenseC_Source_TowerDefenseC_Buff_Base_h_12_SPARSE_DATA
#define TowerDefenseC_Source_TowerDefenseC_Buff_Base_h_12_RPC_WRAPPERS \
	virtual void DeactivateEffect_Implementation(); \
	virtual void ActivateEffect_Implementation(); \
 \
	DECLARE_FUNCTION(execDeactivateEffect); \
	DECLARE_FUNCTION(execActivateEffect); \
	DECLARE_FUNCTION(execSetTarget);


#define TowerDefenseC_Source_TowerDefenseC_Buff_Base_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	virtual void DeactivateEffect_Implementation(); \
	virtual void ActivateEffect_Implementation(); \
 \
	DECLARE_FUNCTION(execDeactivateEffect); \
	DECLARE_FUNCTION(execActivateEffect); \
	DECLARE_FUNCTION(execSetTarget);


#define TowerDefenseC_Source_TowerDefenseC_Buff_Base_h_12_EVENT_PARMS
#define TowerDefenseC_Source_TowerDefenseC_Buff_Base_h_12_CALLBACK_WRAPPERS
#define TowerDefenseC_Source_TowerDefenseC_Buff_Base_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesABuff_Base(); \
	friend struct Z_Construct_UClass_ABuff_Base_Statics; \
public: \
	DECLARE_CLASS(ABuff_Base, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/TowerDefenseC"), NO_API) \
	DECLARE_SERIALIZER(ABuff_Base)


#define TowerDefenseC_Source_TowerDefenseC_Buff_Base_h_12_INCLASS \
private: \
	static void StaticRegisterNativesABuff_Base(); \
	friend struct Z_Construct_UClass_ABuff_Base_Statics; \
public: \
	DECLARE_CLASS(ABuff_Base, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/TowerDefenseC"), NO_API) \
	DECLARE_SERIALIZER(ABuff_Base)


#define TowerDefenseC_Source_TowerDefenseC_Buff_Base_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ABuff_Base(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ABuff_Base) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ABuff_Base); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ABuff_Base); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ABuff_Base(ABuff_Base&&); \
	NO_API ABuff_Base(const ABuff_Base&); \
public:


#define TowerDefenseC_Source_TowerDefenseC_Buff_Base_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ABuff_Base(ABuff_Base&&); \
	NO_API ABuff_Base(const ABuff_Base&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ABuff_Base); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ABuff_Base); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ABuff_Base)


#define TowerDefenseC_Source_TowerDefenseC_Buff_Base_h_12_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__duration() { return STRUCT_OFFSET(ABuff_Base, duration); } \
	FORCEINLINE static uint32 __PPO__Islooping() { return STRUCT_OFFSET(ABuff_Base, Islooping); } \
	FORCEINLINE static uint32 __PPO__Target() { return STRUCT_OFFSET(ABuff_Base, Target); }


#define TowerDefenseC_Source_TowerDefenseC_Buff_Base_h_9_PROLOG \
	TowerDefenseC_Source_TowerDefenseC_Buff_Base_h_12_EVENT_PARMS


#define TowerDefenseC_Source_TowerDefenseC_Buff_Base_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TowerDefenseC_Source_TowerDefenseC_Buff_Base_h_12_PRIVATE_PROPERTY_OFFSET \
	TowerDefenseC_Source_TowerDefenseC_Buff_Base_h_12_SPARSE_DATA \
	TowerDefenseC_Source_TowerDefenseC_Buff_Base_h_12_RPC_WRAPPERS \
	TowerDefenseC_Source_TowerDefenseC_Buff_Base_h_12_CALLBACK_WRAPPERS \
	TowerDefenseC_Source_TowerDefenseC_Buff_Base_h_12_INCLASS \
	TowerDefenseC_Source_TowerDefenseC_Buff_Base_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define TowerDefenseC_Source_TowerDefenseC_Buff_Base_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TowerDefenseC_Source_TowerDefenseC_Buff_Base_h_12_PRIVATE_PROPERTY_OFFSET \
	TowerDefenseC_Source_TowerDefenseC_Buff_Base_h_12_SPARSE_DATA \
	TowerDefenseC_Source_TowerDefenseC_Buff_Base_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	TowerDefenseC_Source_TowerDefenseC_Buff_Base_h_12_CALLBACK_WRAPPERS \
	TowerDefenseC_Source_TowerDefenseC_Buff_Base_h_12_INCLASS_NO_PURE_DECLS \
	TowerDefenseC_Source_TowerDefenseC_Buff_Base_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> TOWERDEFENSEC_API UClass* StaticClass<class ABuff_Base>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID TowerDefenseC_Source_TowerDefenseC_Buff_Base_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
