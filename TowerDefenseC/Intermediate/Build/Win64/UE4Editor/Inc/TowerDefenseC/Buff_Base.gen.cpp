// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "TowerDefenseC/Buff_Base.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeBuff_Base() {}
// Cross Module References
	TOWERDEFENSEC_API UClass* Z_Construct_UClass_ABuff_Base_NoRegister();
	TOWERDEFENSEC_API UClass* Z_Construct_UClass_ABuff_Base();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_TowerDefenseC();
	ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
// End Cross Module References
	DEFINE_FUNCTION(ABuff_Base::execDeactivateEffect)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->DeactivateEffect_Implementation();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ABuff_Base::execActivateEffect)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->ActivateEffect_Implementation();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ABuff_Base::execSetTarget)
	{
		P_GET_OBJECT(AActor,Z_Param_targetToSet);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetTarget(Z_Param_targetToSet);
		P_NATIVE_END;
	}
	static FName NAME_ABuff_Base_ActivateEffect = FName(TEXT("ActivateEffect"));
	void ABuff_Base::ActivateEffect()
	{
		ProcessEvent(FindFunctionChecked(NAME_ABuff_Base_ActivateEffect),NULL);
	}
	static FName NAME_ABuff_Base_DeactivateEffect = FName(TEXT("DeactivateEffect"));
	void ABuff_Base::DeactivateEffect()
	{
		ProcessEvent(FindFunctionChecked(NAME_ABuff_Base_DeactivateEffect),NULL);
	}
	void ABuff_Base::StaticRegisterNativesABuff_Base()
	{
		UClass* Class = ABuff_Base::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "ActivateEffect", &ABuff_Base::execActivateEffect },
			{ "DeactivateEffect", &ABuff_Base::execDeactivateEffect },
			{ "SetTarget", &ABuff_Base::execSetTarget },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_ABuff_Base_ActivateEffect_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ABuff_Base_ActivateEffect_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Buff_Base.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ABuff_Base_ActivateEffect_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ABuff_Base, nullptr, "ActivateEffect", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x0C080C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ABuff_Base_ActivateEffect_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ABuff_Base_ActivateEffect_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ABuff_Base_ActivateEffect()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ABuff_Base_ActivateEffect_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ABuff_Base_DeactivateEffect_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ABuff_Base_DeactivateEffect_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Buff_Base.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ABuff_Base_DeactivateEffect_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ABuff_Base, nullptr, "DeactivateEffect", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x0C080C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ABuff_Base_DeactivateEffect_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ABuff_Base_DeactivateEffect_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ABuff_Base_DeactivateEffect()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ABuff_Base_DeactivateEffect_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ABuff_Base_SetTarget_Statics
	{
		struct Buff_Base_eventSetTarget_Parms
		{
			AActor* targetToSet;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_targetToSet;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ABuff_Base_SetTarget_Statics::NewProp_targetToSet = { "targetToSet", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Buff_Base_eventSetTarget_Parms, targetToSet), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ABuff_Base_SetTarget_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ABuff_Base_SetTarget_Statics::NewProp_targetToSet,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ABuff_Base_SetTarget_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Buff_Base.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ABuff_Base_SetTarget_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ABuff_Base, nullptr, "SetTarget", nullptr, nullptr, sizeof(Buff_Base_eventSetTarget_Parms), Z_Construct_UFunction_ABuff_Base_SetTarget_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ABuff_Base_SetTarget_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04080401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ABuff_Base_SetTarget_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ABuff_Base_SetTarget_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ABuff_Base_SetTarget()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ABuff_Base_SetTarget_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_ABuff_Base_NoRegister()
	{
		return ABuff_Base::StaticClass();
	}
	struct Z_Construct_UClass_ABuff_Base_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Target_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Target;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Islooping_MetaData[];
#endif
		static void NewProp_Islooping_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_Islooping;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_duration_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_duration;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ABuff_Base_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_TowerDefenseC,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_ABuff_Base_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_ABuff_Base_ActivateEffect, "ActivateEffect" }, // 592568591
		{ &Z_Construct_UFunction_ABuff_Base_DeactivateEffect, "DeactivateEffect" }, // 2197455897
		{ &Z_Construct_UFunction_ABuff_Base_SetTarget, "SetTarget" }, // 3904039551
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ABuff_Base_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "Buff_Base.h" },
		{ "ModuleRelativePath", "Buff_Base.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ABuff_Base_Statics::NewProp_Target_MetaData[] = {
		{ "AllowPrivateAccess", "true" },
		{ "Category", "Buff_Base" },
		{ "ModuleRelativePath", "Buff_Base.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ABuff_Base_Statics::NewProp_Target = { "Target", nullptr, (EPropertyFlags)0x0040000000000015, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ABuff_Base, Target), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ABuff_Base_Statics::NewProp_Target_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ABuff_Base_Statics::NewProp_Target_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ABuff_Base_Statics::NewProp_Islooping_MetaData[] = {
		{ "Category", "Buff_Base" },
		{ "ModuleRelativePath", "Buff_Base.h" },
	};
#endif
	void Z_Construct_UClass_ABuff_Base_Statics::NewProp_Islooping_SetBit(void* Obj)
	{
		((ABuff_Base*)Obj)->Islooping = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_ABuff_Base_Statics::NewProp_Islooping = { "Islooping", nullptr, (EPropertyFlags)0x0020080000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(ABuff_Base), &Z_Construct_UClass_ABuff_Base_Statics::NewProp_Islooping_SetBit, METADATA_PARAMS(Z_Construct_UClass_ABuff_Base_Statics::NewProp_Islooping_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ABuff_Base_Statics::NewProp_Islooping_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ABuff_Base_Statics::NewProp_duration_MetaData[] = {
		{ "Category", "Buff_Base" },
		{ "ModuleRelativePath", "Buff_Base.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ABuff_Base_Statics::NewProp_duration = { "duration", nullptr, (EPropertyFlags)0x0020080000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ABuff_Base, duration), METADATA_PARAMS(Z_Construct_UClass_ABuff_Base_Statics::NewProp_duration_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ABuff_Base_Statics::NewProp_duration_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ABuff_Base_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ABuff_Base_Statics::NewProp_Target,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ABuff_Base_Statics::NewProp_Islooping,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ABuff_Base_Statics::NewProp_duration,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ABuff_Base_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ABuff_Base>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ABuff_Base_Statics::ClassParams = {
		&ABuff_Base::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_ABuff_Base_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_ABuff_Base_Statics::PropPointers),
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_ABuff_Base_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ABuff_Base_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ABuff_Base()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ABuff_Base_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ABuff_Base, 2420795594);
	template<> TOWERDEFENSEC_API UClass* StaticClass<ABuff_Base>()
	{
		return ABuff_Base::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ABuff_Base(Z_Construct_UClass_ABuff_Base, &ABuff_Base::StaticClass, TEXT("/Script/TowerDefenseC"), TEXT("ABuff_Base"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ABuff_Base);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
