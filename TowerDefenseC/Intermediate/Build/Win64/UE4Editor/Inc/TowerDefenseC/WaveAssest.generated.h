// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef TOWERDEFENSEC_WaveAssest_generated_h
#error "WaveAssest.generated.h already included, missing '#pragma once' in WaveAssest.h"
#endif
#define TOWERDEFENSEC_WaveAssest_generated_h

#define TowerDefenseC_Source_TowerDefenseC_WaveAssest_h_16_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FSpawnDetails_Statics; \
	TOWERDEFENSEC_API static class UScriptStruct* StaticStruct();


template<> TOWERDEFENSEC_API UScriptStruct* StaticStruct<struct FSpawnDetails>();

#define TowerDefenseC_Source_TowerDefenseC_WaveAssest_h_29_SPARSE_DATA
#define TowerDefenseC_Source_TowerDefenseC_WaveAssest_h_29_RPC_WRAPPERS
#define TowerDefenseC_Source_TowerDefenseC_WaveAssest_h_29_RPC_WRAPPERS_NO_PURE_DECLS
#define TowerDefenseC_Source_TowerDefenseC_WaveAssest_h_29_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUWaveAssest(); \
	friend struct Z_Construct_UClass_UWaveAssest_Statics; \
public: \
	DECLARE_CLASS(UWaveAssest, UDataAsset, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/TowerDefenseC"), NO_API) \
	DECLARE_SERIALIZER(UWaveAssest)


#define TowerDefenseC_Source_TowerDefenseC_WaveAssest_h_29_INCLASS \
private: \
	static void StaticRegisterNativesUWaveAssest(); \
	friend struct Z_Construct_UClass_UWaveAssest_Statics; \
public: \
	DECLARE_CLASS(UWaveAssest, UDataAsset, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/TowerDefenseC"), NO_API) \
	DECLARE_SERIALIZER(UWaveAssest)


#define TowerDefenseC_Source_TowerDefenseC_WaveAssest_h_29_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UWaveAssest(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UWaveAssest) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UWaveAssest); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UWaveAssest); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UWaveAssest(UWaveAssest&&); \
	NO_API UWaveAssest(const UWaveAssest&); \
public:


#define TowerDefenseC_Source_TowerDefenseC_WaveAssest_h_29_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UWaveAssest(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UWaveAssest(UWaveAssest&&); \
	NO_API UWaveAssest(const UWaveAssest&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UWaveAssest); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UWaveAssest); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UWaveAssest)


#define TowerDefenseC_Source_TowerDefenseC_WaveAssest_h_29_PRIVATE_PROPERTY_OFFSET
#define TowerDefenseC_Source_TowerDefenseC_WaveAssest_h_26_PROLOG
#define TowerDefenseC_Source_TowerDefenseC_WaveAssest_h_29_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TowerDefenseC_Source_TowerDefenseC_WaveAssest_h_29_PRIVATE_PROPERTY_OFFSET \
	TowerDefenseC_Source_TowerDefenseC_WaveAssest_h_29_SPARSE_DATA \
	TowerDefenseC_Source_TowerDefenseC_WaveAssest_h_29_RPC_WRAPPERS \
	TowerDefenseC_Source_TowerDefenseC_WaveAssest_h_29_INCLASS \
	TowerDefenseC_Source_TowerDefenseC_WaveAssest_h_29_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define TowerDefenseC_Source_TowerDefenseC_WaveAssest_h_29_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TowerDefenseC_Source_TowerDefenseC_WaveAssest_h_29_PRIVATE_PROPERTY_OFFSET \
	TowerDefenseC_Source_TowerDefenseC_WaveAssest_h_29_SPARSE_DATA \
	TowerDefenseC_Source_TowerDefenseC_WaveAssest_h_29_RPC_WRAPPERS_NO_PURE_DECLS \
	TowerDefenseC_Source_TowerDefenseC_WaveAssest_h_29_INCLASS_NO_PURE_DECLS \
	TowerDefenseC_Source_TowerDefenseC_WaveAssest_h_29_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> TOWERDEFENSEC_API UClass* StaticClass<class UWaveAssest>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID TowerDefenseC_Source_TowerDefenseC_WaveAssest_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
