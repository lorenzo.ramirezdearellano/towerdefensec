// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "TowerDefenseC/Tower_Info.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeTower_Info() {}
// Cross Module References
	TOWERDEFENSEC_API UClass* Z_Construct_UClass_UTower_Info_NoRegister();
	TOWERDEFENSEC_API UClass* Z_Construct_UClass_UTower_Info();
	ENGINE_API UClass* Z_Construct_UClass_UDataAsset();
	UPackage* Z_Construct_UPackage__Script_TowerDefenseC();
// End Cross Module References
	void UTower_Info::StaticRegisterNativesUTower_Info()
	{
	}
	UClass* Z_Construct_UClass_UTower_Info_NoRegister()
	{
		return UTower_Info::StaticClass();
	}
	struct Z_Construct_UClass_UTower_Info_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UTower_Info_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDataAsset,
		(UObject* (*)())Z_Construct_UPackage__Script_TowerDefenseC,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTower_Info_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "IncludePath", "Tower_Info.h" },
		{ "ModuleRelativePath", "Tower_Info.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UTower_Info_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UTower_Info>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UTower_Info_Statics::ClassParams = {
		&UTower_Info::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UTower_Info_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UTower_Info_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UTower_Info()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UTower_Info_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UTower_Info, 2156676589);
	template<> TOWERDEFENSEC_API UClass* StaticClass<UTower_Info>()
	{
		return UTower_Info::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UTower_Info(Z_Construct_UClass_UTower_Info, &UTower_Info::StaticClass, TEXT("/Script/TowerDefenseC"), TEXT("UTower_Info"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UTower_Info);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
