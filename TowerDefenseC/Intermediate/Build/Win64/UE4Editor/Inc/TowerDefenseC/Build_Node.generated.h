// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef TOWERDEFENSEC_Build_Node_generated_h
#error "Build_Node.generated.h already included, missing '#pragma once' in Build_Node.h"
#endif
#define TOWERDEFENSEC_Build_Node_generated_h

#define TowerDefenseC_Source_TowerDefenseC_Build_Node_h_13_SPARSE_DATA
#define TowerDefenseC_Source_TowerDefenseC_Build_Node_h_13_RPC_WRAPPERS
#define TowerDefenseC_Source_TowerDefenseC_Build_Node_h_13_RPC_WRAPPERS_NO_PURE_DECLS
#define TowerDefenseC_Source_TowerDefenseC_Build_Node_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesABuild_Node(); \
	friend struct Z_Construct_UClass_ABuild_Node_Statics; \
public: \
	DECLARE_CLASS(ABuild_Node, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/TowerDefenseC"), NO_API) \
	DECLARE_SERIALIZER(ABuild_Node)


#define TowerDefenseC_Source_TowerDefenseC_Build_Node_h_13_INCLASS \
private: \
	static void StaticRegisterNativesABuild_Node(); \
	friend struct Z_Construct_UClass_ABuild_Node_Statics; \
public: \
	DECLARE_CLASS(ABuild_Node, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/TowerDefenseC"), NO_API) \
	DECLARE_SERIALIZER(ABuild_Node)


#define TowerDefenseC_Source_TowerDefenseC_Build_Node_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ABuild_Node(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ABuild_Node) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ABuild_Node); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ABuild_Node); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ABuild_Node(ABuild_Node&&); \
	NO_API ABuild_Node(const ABuild_Node&); \
public:


#define TowerDefenseC_Source_TowerDefenseC_Build_Node_h_13_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ABuild_Node(ABuild_Node&&); \
	NO_API ABuild_Node(const ABuild_Node&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ABuild_Node); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ABuild_Node); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ABuild_Node)


#define TowerDefenseC_Source_TowerDefenseC_Build_Node_h_13_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__CurrentTower() { return STRUCT_OFFSET(ABuild_Node, CurrentTower); }


#define TowerDefenseC_Source_TowerDefenseC_Build_Node_h_10_PROLOG
#define TowerDefenseC_Source_TowerDefenseC_Build_Node_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TowerDefenseC_Source_TowerDefenseC_Build_Node_h_13_PRIVATE_PROPERTY_OFFSET \
	TowerDefenseC_Source_TowerDefenseC_Build_Node_h_13_SPARSE_DATA \
	TowerDefenseC_Source_TowerDefenseC_Build_Node_h_13_RPC_WRAPPERS \
	TowerDefenseC_Source_TowerDefenseC_Build_Node_h_13_INCLASS \
	TowerDefenseC_Source_TowerDefenseC_Build_Node_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define TowerDefenseC_Source_TowerDefenseC_Build_Node_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TowerDefenseC_Source_TowerDefenseC_Build_Node_h_13_PRIVATE_PROPERTY_OFFSET \
	TowerDefenseC_Source_TowerDefenseC_Build_Node_h_13_SPARSE_DATA \
	TowerDefenseC_Source_TowerDefenseC_Build_Node_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	TowerDefenseC_Source_TowerDefenseC_Build_Node_h_13_INCLASS_NO_PURE_DECLS \
	TowerDefenseC_Source_TowerDefenseC_Build_Node_h_13_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> TOWERDEFENSEC_API UClass* StaticClass<class ABuild_Node>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID TowerDefenseC_Source_TowerDefenseC_Build_Node_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
