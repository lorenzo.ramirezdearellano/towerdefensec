// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef TOWERDEFENSEC_Tower_Info_generated_h
#error "Tower_Info.generated.h already included, missing '#pragma once' in Tower_Info.h"
#endif
#define TOWERDEFENSEC_Tower_Info_generated_h

#define TowerDefenseC_Source_TowerDefenseC_Tower_Info_h_17_SPARSE_DATA
#define TowerDefenseC_Source_TowerDefenseC_Tower_Info_h_17_RPC_WRAPPERS
#define TowerDefenseC_Source_TowerDefenseC_Tower_Info_h_17_RPC_WRAPPERS_NO_PURE_DECLS
#define TowerDefenseC_Source_TowerDefenseC_Tower_Info_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUTower_Info(); \
	friend struct Z_Construct_UClass_UTower_Info_Statics; \
public: \
	DECLARE_CLASS(UTower_Info, UDataAsset, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/TowerDefenseC"), NO_API) \
	DECLARE_SERIALIZER(UTower_Info)


#define TowerDefenseC_Source_TowerDefenseC_Tower_Info_h_17_INCLASS \
private: \
	static void StaticRegisterNativesUTower_Info(); \
	friend struct Z_Construct_UClass_UTower_Info_Statics; \
public: \
	DECLARE_CLASS(UTower_Info, UDataAsset, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/TowerDefenseC"), NO_API) \
	DECLARE_SERIALIZER(UTower_Info)


#define TowerDefenseC_Source_TowerDefenseC_Tower_Info_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UTower_Info(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UTower_Info) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UTower_Info); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UTower_Info); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UTower_Info(UTower_Info&&); \
	NO_API UTower_Info(const UTower_Info&); \
public:


#define TowerDefenseC_Source_TowerDefenseC_Tower_Info_h_17_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UTower_Info(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UTower_Info(UTower_Info&&); \
	NO_API UTower_Info(const UTower_Info&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UTower_Info); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UTower_Info); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UTower_Info)


#define TowerDefenseC_Source_TowerDefenseC_Tower_Info_h_17_PRIVATE_PROPERTY_OFFSET
#define TowerDefenseC_Source_TowerDefenseC_Tower_Info_h_14_PROLOG
#define TowerDefenseC_Source_TowerDefenseC_Tower_Info_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TowerDefenseC_Source_TowerDefenseC_Tower_Info_h_17_PRIVATE_PROPERTY_OFFSET \
	TowerDefenseC_Source_TowerDefenseC_Tower_Info_h_17_SPARSE_DATA \
	TowerDefenseC_Source_TowerDefenseC_Tower_Info_h_17_RPC_WRAPPERS \
	TowerDefenseC_Source_TowerDefenseC_Tower_Info_h_17_INCLASS \
	TowerDefenseC_Source_TowerDefenseC_Tower_Info_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define TowerDefenseC_Source_TowerDefenseC_Tower_Info_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TowerDefenseC_Source_TowerDefenseC_Tower_Info_h_17_PRIVATE_PROPERTY_OFFSET \
	TowerDefenseC_Source_TowerDefenseC_Tower_Info_h_17_SPARSE_DATA \
	TowerDefenseC_Source_TowerDefenseC_Tower_Info_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	TowerDefenseC_Source_TowerDefenseC_Tower_Info_h_17_INCLASS_NO_PURE_DECLS \
	TowerDefenseC_Source_TowerDefenseC_Tower_Info_h_17_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> TOWERDEFENSEC_API UClass* StaticClass<class UTower_Info>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID TowerDefenseC_Source_TowerDefenseC_Tower_Info_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
