// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "TowerDefenseC/PlayerActor.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodePlayerActor() {}
// Cross Module References
	TOWERDEFENSEC_API UClass* Z_Construct_UClass_APlayerActor_NoRegister();
	TOWERDEFENSEC_API UClass* Z_Construct_UClass_APlayerActor();
	ENGINE_API UClass* Z_Construct_UClass_ACharacter();
	UPackage* Z_Construct_UPackage__Script_TowerDefenseC();
// End Cross Module References
	DEFINE_FUNCTION(APlayerActor::execDamageTaken)
	{
		P_GET_PROPERTY(FIntProperty,Z_Param_damage);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->DamageTaken(Z_Param_damage);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(APlayerActor::execAddPlayerGold)
	{
		P_GET_PROPERTY(FIntProperty,Z_Param_goldToAdd);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->AddPlayerGold(Z_Param_goldToAdd);
		P_NATIVE_END;
	}
	void APlayerActor::StaticRegisterNativesAPlayerActor()
	{
		UClass* Class = APlayerActor::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "AddPlayerGold", &APlayerActor::execAddPlayerGold },
			{ "DamageTaken", &APlayerActor::execDamageTaken },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_APlayerActor_AddPlayerGold_Statics
	{
		struct PlayerActor_eventAddPlayerGold_Parms
		{
			int32 goldToAdd;
		};
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_goldToAdd;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_APlayerActor_AddPlayerGold_Statics::NewProp_goldToAdd = { "goldToAdd", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PlayerActor_eventAddPlayerGold_Parms, goldToAdd), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_APlayerActor_AddPlayerGold_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_APlayerActor_AddPlayerGold_Statics::NewProp_goldToAdd,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_APlayerActor_AddPlayerGold_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "PlayerActor.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_APlayerActor_AddPlayerGold_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_APlayerActor, nullptr, "AddPlayerGold", nullptr, nullptr, sizeof(PlayerActor_eventAddPlayerGold_Parms), Z_Construct_UFunction_APlayerActor_AddPlayerGold_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_APlayerActor_AddPlayerGold_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_APlayerActor_AddPlayerGold_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_APlayerActor_AddPlayerGold_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_APlayerActor_AddPlayerGold()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_APlayerActor_AddPlayerGold_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_APlayerActor_DamageTaken_Statics
	{
		struct PlayerActor_eventDamageTaken_Parms
		{
			int32 damage;
		};
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_damage;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_APlayerActor_DamageTaken_Statics::NewProp_damage = { "damage", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PlayerActor_eventDamageTaken_Parms, damage), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_APlayerActor_DamageTaken_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_APlayerActor_DamageTaken_Statics::NewProp_damage,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_APlayerActor_DamageTaken_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "PlayerActor.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_APlayerActor_DamageTaken_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_APlayerActor, nullptr, "DamageTaken", nullptr, nullptr, sizeof(PlayerActor_eventDamageTaken_Parms), Z_Construct_UFunction_APlayerActor_DamageTaken_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_APlayerActor_DamageTaken_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_APlayerActor_DamageTaken_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_APlayerActor_DamageTaken_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_APlayerActor_DamageTaken()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_APlayerActor_DamageTaken_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_APlayerActor_NoRegister()
	{
		return APlayerActor::StaticClass();
	}
	struct Z_Construct_UClass_APlayerActor_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_playerHealth_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_playerHealth;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_playerGold_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_playerGold;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_APlayerActor_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_ACharacter,
		(UObject* (*)())Z_Construct_UPackage__Script_TowerDefenseC,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_APlayerActor_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_APlayerActor_AddPlayerGold, "AddPlayerGold" }, // 2407873408
		{ &Z_Construct_UFunction_APlayerActor_DamageTaken, "DamageTaken" }, // 2869824949
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APlayerActor_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Navigation" },
		{ "IncludePath", "PlayerActor.h" },
		{ "ModuleRelativePath", "PlayerActor.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APlayerActor_Statics::NewProp_playerHealth_MetaData[] = {
		{ "Category", "PlayerActor" },
		{ "ModuleRelativePath", "PlayerActor.h" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_APlayerActor_Statics::NewProp_playerHealth = { "playerHealth", nullptr, (EPropertyFlags)0x0020080000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APlayerActor, playerHealth), METADATA_PARAMS(Z_Construct_UClass_APlayerActor_Statics::NewProp_playerHealth_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_APlayerActor_Statics::NewProp_playerHealth_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APlayerActor_Statics::NewProp_playerGold_MetaData[] = {
		{ "Category", "PlayerActor" },
		{ "ModuleRelativePath", "PlayerActor.h" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_APlayerActor_Statics::NewProp_playerGold = { "playerGold", nullptr, (EPropertyFlags)0x0020080000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APlayerActor, playerGold), METADATA_PARAMS(Z_Construct_UClass_APlayerActor_Statics::NewProp_playerGold_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_APlayerActor_Statics::NewProp_playerGold_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_APlayerActor_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APlayerActor_Statics::NewProp_playerHealth,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APlayerActor_Statics::NewProp_playerGold,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_APlayerActor_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<APlayerActor>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_APlayerActor_Statics::ClassParams = {
		&APlayerActor::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_APlayerActor_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_APlayerActor_Statics::PropPointers),
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_APlayerActor_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_APlayerActor_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_APlayerActor()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_APlayerActor_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(APlayerActor, 1649207892);
	template<> TOWERDEFENSEC_API UClass* StaticClass<APlayerActor>()
	{
		return APlayerActor::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_APlayerActor(Z_Construct_UClass_APlayerActor, &APlayerActor::StaticClass, TEXT("/Script/TowerDefenseC"), TEXT("APlayerActor"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(APlayerActor);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
