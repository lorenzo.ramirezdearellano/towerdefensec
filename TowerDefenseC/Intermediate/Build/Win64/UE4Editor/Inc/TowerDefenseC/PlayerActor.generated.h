// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef TOWERDEFENSEC_PlayerActor_generated_h
#error "PlayerActor.generated.h already included, missing '#pragma once' in PlayerActor.h"
#endif
#define TOWERDEFENSEC_PlayerActor_generated_h

#define TowerDefenseC_Source_TowerDefenseC_PlayerActor_h_12_SPARSE_DATA
#define TowerDefenseC_Source_TowerDefenseC_PlayerActor_h_12_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execDamageTaken); \
	DECLARE_FUNCTION(execAddPlayerGold);


#define TowerDefenseC_Source_TowerDefenseC_PlayerActor_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execDamageTaken); \
	DECLARE_FUNCTION(execAddPlayerGold);


#define TowerDefenseC_Source_TowerDefenseC_PlayerActor_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAPlayerActor(); \
	friend struct Z_Construct_UClass_APlayerActor_Statics; \
public: \
	DECLARE_CLASS(APlayerActor, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/TowerDefenseC"), NO_API) \
	DECLARE_SERIALIZER(APlayerActor)


#define TowerDefenseC_Source_TowerDefenseC_PlayerActor_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAPlayerActor(); \
	friend struct Z_Construct_UClass_APlayerActor_Statics; \
public: \
	DECLARE_CLASS(APlayerActor, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/TowerDefenseC"), NO_API) \
	DECLARE_SERIALIZER(APlayerActor)


#define TowerDefenseC_Source_TowerDefenseC_PlayerActor_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API APlayerActor(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APlayerActor) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APlayerActor); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APlayerActor); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APlayerActor(APlayerActor&&); \
	NO_API APlayerActor(const APlayerActor&); \
public:


#define TowerDefenseC_Source_TowerDefenseC_PlayerActor_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APlayerActor(APlayerActor&&); \
	NO_API APlayerActor(const APlayerActor&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APlayerActor); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APlayerActor); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(APlayerActor)


#define TowerDefenseC_Source_TowerDefenseC_PlayerActor_h_12_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__playerGold() { return STRUCT_OFFSET(APlayerActor, playerGold); } \
	FORCEINLINE static uint32 __PPO__playerHealth() { return STRUCT_OFFSET(APlayerActor, playerHealth); }


#define TowerDefenseC_Source_TowerDefenseC_PlayerActor_h_9_PROLOG
#define TowerDefenseC_Source_TowerDefenseC_PlayerActor_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TowerDefenseC_Source_TowerDefenseC_PlayerActor_h_12_PRIVATE_PROPERTY_OFFSET \
	TowerDefenseC_Source_TowerDefenseC_PlayerActor_h_12_SPARSE_DATA \
	TowerDefenseC_Source_TowerDefenseC_PlayerActor_h_12_RPC_WRAPPERS \
	TowerDefenseC_Source_TowerDefenseC_PlayerActor_h_12_INCLASS \
	TowerDefenseC_Source_TowerDefenseC_PlayerActor_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define TowerDefenseC_Source_TowerDefenseC_PlayerActor_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TowerDefenseC_Source_TowerDefenseC_PlayerActor_h_12_PRIVATE_PROPERTY_OFFSET \
	TowerDefenseC_Source_TowerDefenseC_PlayerActor_h_12_SPARSE_DATA \
	TowerDefenseC_Source_TowerDefenseC_PlayerActor_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	TowerDefenseC_Source_TowerDefenseC_PlayerActor_h_12_INCLASS_NO_PURE_DECLS \
	TowerDefenseC_Source_TowerDefenseC_PlayerActor_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> TOWERDEFENSEC_API UClass* StaticClass<class APlayerActor>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID TowerDefenseC_Source_TowerDefenseC_PlayerActor_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
