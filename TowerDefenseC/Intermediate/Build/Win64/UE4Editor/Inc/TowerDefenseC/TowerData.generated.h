// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef TOWERDEFENSEC_TowerData_generated_h
#error "TowerData.generated.h already included, missing '#pragma once' in TowerData.h"
#endif
#define TOWERDEFENSEC_TowerData_generated_h

#define TowerDefenseC_Source_TowerDefenseC_TowerData_h_16_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FStatLevels_Statics; \
	TOWERDEFENSEC_API static class UScriptStruct* StaticStruct();


template<> TOWERDEFENSEC_API UScriptStruct* StaticStruct<struct FStatLevels>();

#define TowerDefenseC_Source_TowerDefenseC_TowerData_h_32_SPARSE_DATA
#define TowerDefenseC_Source_TowerDefenseC_TowerData_h_32_RPC_WRAPPERS
#define TowerDefenseC_Source_TowerDefenseC_TowerData_h_32_RPC_WRAPPERS_NO_PURE_DECLS
#define TowerDefenseC_Source_TowerDefenseC_TowerData_h_32_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUTowerData(); \
	friend struct Z_Construct_UClass_UTowerData_Statics; \
public: \
	DECLARE_CLASS(UTowerData, UDataAsset, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/TowerDefenseC"), NO_API) \
	DECLARE_SERIALIZER(UTowerData)


#define TowerDefenseC_Source_TowerDefenseC_TowerData_h_32_INCLASS \
private: \
	static void StaticRegisterNativesUTowerData(); \
	friend struct Z_Construct_UClass_UTowerData_Statics; \
public: \
	DECLARE_CLASS(UTowerData, UDataAsset, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/TowerDefenseC"), NO_API) \
	DECLARE_SERIALIZER(UTowerData)


#define TowerDefenseC_Source_TowerDefenseC_TowerData_h_32_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UTowerData(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UTowerData) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UTowerData); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UTowerData); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UTowerData(UTowerData&&); \
	NO_API UTowerData(const UTowerData&); \
public:


#define TowerDefenseC_Source_TowerDefenseC_TowerData_h_32_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UTowerData(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UTowerData(UTowerData&&); \
	NO_API UTowerData(const UTowerData&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UTowerData); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UTowerData); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UTowerData)


#define TowerDefenseC_Source_TowerDefenseC_TowerData_h_32_PRIVATE_PROPERTY_OFFSET
#define TowerDefenseC_Source_TowerDefenseC_TowerData_h_29_PROLOG
#define TowerDefenseC_Source_TowerDefenseC_TowerData_h_32_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TowerDefenseC_Source_TowerDefenseC_TowerData_h_32_PRIVATE_PROPERTY_OFFSET \
	TowerDefenseC_Source_TowerDefenseC_TowerData_h_32_SPARSE_DATA \
	TowerDefenseC_Source_TowerDefenseC_TowerData_h_32_RPC_WRAPPERS \
	TowerDefenseC_Source_TowerDefenseC_TowerData_h_32_INCLASS \
	TowerDefenseC_Source_TowerDefenseC_TowerData_h_32_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define TowerDefenseC_Source_TowerDefenseC_TowerData_h_32_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TowerDefenseC_Source_TowerDefenseC_TowerData_h_32_PRIVATE_PROPERTY_OFFSET \
	TowerDefenseC_Source_TowerDefenseC_TowerData_h_32_SPARSE_DATA \
	TowerDefenseC_Source_TowerDefenseC_TowerData_h_32_RPC_WRAPPERS_NO_PURE_DECLS \
	TowerDefenseC_Source_TowerDefenseC_TowerData_h_32_INCLASS_NO_PURE_DECLS \
	TowerDefenseC_Source_TowerDefenseC_TowerData_h_32_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> TOWERDEFENSEC_API UClass* StaticClass<class UTowerData>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID TowerDefenseC_Source_TowerDefenseC_TowerData_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
