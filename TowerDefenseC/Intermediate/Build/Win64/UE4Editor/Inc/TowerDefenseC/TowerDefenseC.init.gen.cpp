// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeTowerDefenseC_init() {}
	TOWERDEFENSEC_API UFunction* Z_Construct_UDelegateFunction_TowerDefenseC_spawned__DelegateSignature();
	TOWERDEFENSEC_API UFunction* Z_Construct_UDelegateFunction_TowerDefenseC_WaveStart__DelegateSignature();
	TOWERDEFENSEC_API UFunction* Z_Construct_UDelegateFunction_TowerDefenseC_enemyDefeated__DelegateSignature();
	TOWERDEFENSEC_API UFunction* Z_Construct_UDelegateFunction_TowerDefenseC_waveClear__DelegateSignature();
	TOWERDEFENSEC_API UFunction* Z_Construct_UDelegateFunction_TowerDefenseC_destroyedSignature__DelegateSignature();
	UPackage* Z_Construct_UPackage__Script_TowerDefenseC()
	{
		static UPackage* ReturnPackage = nullptr;
		if (!ReturnPackage)
		{
			static UObject* (*const SingletonFuncArray[])() = {
				(UObject* (*)())Z_Construct_UDelegateFunction_TowerDefenseC_spawned__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_TowerDefenseC_WaveStart__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_TowerDefenseC_enemyDefeated__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_TowerDefenseC_waveClear__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_TowerDefenseC_destroyedSignature__DelegateSignature,
			};
			static const UE4CodeGen_Private::FPackageParams PackageParams = {
				"/Script/TowerDefenseC",
				SingletonFuncArray,
				UE_ARRAY_COUNT(SingletonFuncArray),
				PKG_CompiledIn | 0x00000000,
				0x9C2E3BDC,
				0xA405C1B5,
				METADATA_PARAMS(nullptr, 0)
			};
			UE4CodeGen_Private::ConstructUPackage(ReturnPackage, PackageParams);
		}
		return ReturnPackage;
	}
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
