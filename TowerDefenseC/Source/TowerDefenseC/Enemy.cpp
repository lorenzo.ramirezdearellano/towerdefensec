// Fill out your copyright notice in the Description page of Project Settings.


#include "Enemy.h"
#include "Kismet/GameplayStatics.h"
#include "WaypointMovement.h"
#include "MyAIController.h"

// Sets default values
AEnemy::AEnemy()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Static Mesh"));
	HitBox = CreateDefaultSubobject<UBoxComponent>(TEXT("HitBox"));
	StaticMesh->SetupAttachment(RootComponent);
	HitBox->SetupAttachment(RootComponent);
	healthComponent = CreateDefaultSubobject<UHealth>(TEXT("Health"));

}

void AEnemy::SetKillReward(int RewardAmt)
{
	KillReward += RewardAmt;
}

// Called when the game starts or when spawned
void AEnemy::BeginPlay()
{
	Super::BeginPlay();

	StaticMesh->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);
	HitBox->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);
	ATDGameMode* gameMode = Cast<ATDGameMode>(UGameplayStatics::GetGameMode(GetWorld()));
	healthComponent->BuffHealth(gameMode->GetDifficultyMultiplier());
}

void AEnemy::Die()
{
	UE_LOG(LogTemp, Warning, TEXT("DestroyingActor"));
	OnDeath.Broadcast(this,KillReward);
	Destroy();
}

// Called every frame
void AEnemy::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void AEnemy::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

void AEnemy::SetWaypoint()
{
	UE_LOG(LogTemp, Warning, TEXT("Moving"));
	class AMyAIController* Control = Cast<AMyAIController>(GetController());

	if (Control) {
		if (currentWaypoint <= Waypoints.Num()) {
			for (AActor* waypoint : Waypoints) {
				if (waypoint == Waypoints[currentWaypoint]) {
					UE_LOG(LogTemp, Warning, TEXT("Waypoint Checked Moving"));
					Control->MoveToActor(waypoint, 100.f, false);
					break;
				}
			}
		}
	}

	if (currentWaypoint < Waypoints.Num()-1) {
		currentWaypoint++;
	}
}

void AEnemy::SetPath(class AActor* waypoint)
{
	Waypoints.Add(waypoint);
	currentWaypoint = 0;
}




