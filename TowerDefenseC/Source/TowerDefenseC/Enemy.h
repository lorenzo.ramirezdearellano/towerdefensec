// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Health.h"
#include "TDGameMode.h"
#include "Components/BoxComponent.h"
#include "Enemy.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FdestroyedSignature, AEnemy*, enemy, int, KillReward);

UENUM(BlueprintType)
enum class EnemyType :uint8 {
	GROUND UMETA(DisplayName = "Ground Unit"),
	FLYING UMETA(DisplayName = "Flying Unit"),
	BOSS UMETA(DisplayName = "Boss Unit")

};

UCLASS()
class TOWERDEFENSEC_API AEnemy : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	AEnemy();

		FdestroyedSignature OnDeath;

		UFUNCTION(BlueprintCallable)
			void Die();
		UPROPERTY(BlueprintReadWrite)
			bool isBurning;
		UPROPERTY(BlueprintReadWrite)
			bool isSlowed;
		UFUNCTION(BlueprintCallable)
			void SetKillReward(int RewardAmt);
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;


	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class UStaticMeshComponent* StaticMesh;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class UBoxComponent* HitBox;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		EnemyType currentType;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class UHealth* healthComponent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int KillReward;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	void SetWaypoint();

	void SetPath(class AActor* waypoint);


private:
	UPROPERTY(EditAnywhere, meta = (AllowPrivateAccess = "true"));
	int currentWaypoint;

	UPROPERTY(EditAnywhere, meta = (AllowPrivateAccess = "true"));
	TArray<AActor*> Waypoints;



};
