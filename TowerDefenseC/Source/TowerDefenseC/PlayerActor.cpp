// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerActor.h"

// Sets default values
APlayerActor::APlayerActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

int APlayerActor::GetPlayerGold()
{
	return playerGold;
}

void APlayerActor::AddPlayerGold(int goldToAdd)
{
	playerGold += goldToAdd;
}

void APlayerActor::DamageTaken(int damage)
{
	UE_LOG(LogTemp, Warning, TEXT("TakingDamage"));
	playerHealth -= damage;
}

// Called when the game starts or when spawned
void APlayerActor::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void APlayerActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

