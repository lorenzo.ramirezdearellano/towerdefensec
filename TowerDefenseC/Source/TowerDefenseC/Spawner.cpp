// Fill out your copyright notice in the Description page of Project Settings.


#include "Spawner.h"

// Sets default values
ASpawner::ASpawner()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SpawnArea = CreateDefaultSubobject<UBoxComponent>(TEXT("SpawnArea"));


}

// Called when the game starts or when spawned
void ASpawner::BeginPlay()
{
	Super::BeginPlay();
	
	//ATDGameMode* gameMode = Cast<ATDGameMode>(UGameplayStatics::GetGameMode(GetWorld()));
	//if (gameMode) {
	//	UE_LOG(LogTemp, Warning, TEXT("Recieving Wave Data"));
	//	gameMode->waveStart.AddDynamic(this, &ASpawner::WaveStarting);
	//}
}

void ASpawner::SpawnEnemy()
{
	GetWorld()->GetTimerManager().ClearAllTimersForObject(this);
	if (currentIndex < Wave->NumOfSpawns) {
		if (Wave->EnemySpawnConfig[currentIndex].SpawnPointIndex == SpawnerIndex) {
			FTransform SpawnLocation = FTransform(SpawnArea->GetComponentRotation(), SpawnArea->GetComponentLocation());
			class AEnemy* SpawnedEnemy = GetWorld()->SpawnActor<class AEnemy>(Wave->EnemySpawnConfig[currentIndex].EnemyType, SpawnLocation);
			for (AActor* waypoint : SpawnerWaypoints) {
				SpawnedEnemy->SetPath(waypoint);
			}
			SpawnedEnemy->SetWaypoint();
			SpawnedEnemy->SetKillReward(Wave->KillReward);
			enemySpawned.Broadcast(SpawnedEnemy,this);
			GetWorld()->GetTimerManager().SetTimer(SpawnDelay, this, &ASpawner::SpawnEnemy, Wave->EnemySpawnConfig[currentIndex].SpawnDelay);
		}
		else {
			GetWorld()->GetTimerManager().SetTimer(SpawnDelay, this, &ASpawner::SpawnEnemy, 1.0f);
		}
		currentIndex++;
	}

}

void ASpawner::WaveStarting(UWaveAssest* WaveData)
{
	Wave = WaveData;
	UE_LOG(LogTemp, Warning, TEXT("Recieved Wave Data"));
	currentIndex = 0;
	SpawnEnemy();
}


// Called every frame
void ASpawner::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

