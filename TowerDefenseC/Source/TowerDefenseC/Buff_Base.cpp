// Fill out your copyright notice in the Description page of Project Settings.


#include "Buff_Base.h"

// Sets default values
ABuff_Base::ABuff_Base()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ABuff_Base::BeginPlay()
{
	Super::BeginPlay();
	SetLifeSpan(duration);
	GetWorld()->GetTimerManager().SetTimer(buffLifespan, this, &ABuff_Base::DeactivateEffect, duration-0.1, Islooping);
}

void ABuff_Base::SetTarget(AActor* targetToSet)
{
	Target = targetToSet;
}

void ABuff_Base::ActivateEffect_Implementation()
{

}

void ABuff_Base::DeactivateEffect_Implementation()
{

}

// Called every frame
void ABuff_Base::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

