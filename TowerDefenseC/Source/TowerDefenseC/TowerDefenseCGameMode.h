// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "TowerDefenseCGameMode.generated.h"

UCLASS(minimalapi)
class ATowerDefenseCGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ATowerDefenseCGameMode();
};



