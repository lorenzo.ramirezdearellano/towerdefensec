// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TDGameMode.h"
#include "Kismet/GameplayStatics.h"
#include "Enemy.h"
#include "WaveAssest.h"
#include "GameFramework/Actor.h"
#include "Spawner.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(Fspawned, AEnemy*, enemy, ASpawner*, spawnLocation);

UCLASS()
class TOWERDEFENSEC_API ASpawner : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASpawner();
	
	Fspawned enemySpawned;

	FTimerHandle SpawnDelay;

	void WaveStarting(UWaveAssest* WaveData);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere)
		class UBoxComponent* SpawnArea;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int SpawnerIndex;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UWaveAssest* Wave;

	UFUNCTION(BlueprintCallable)
		void SpawnEnemy();

	 int currentIndex=0;
public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"));
		TArray<AActor*> SpawnerWaypoints;


};

