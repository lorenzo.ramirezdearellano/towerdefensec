// Fill out your copyright notice in the Description page of Project Settings.


#include "Projectile.h"

// Sets default values
AProjectile::AProjectile()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Static Mesh"));
	HitBox = CreateDefaultSubobject<UBoxComponent>(TEXT("HitBox"));
	projectileMovement = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("Projectile Movement"));
	RootComponent=StaticMesh;
	HitBox->SetupAttachment(RootComponent);

}

// Called when the game starts or when spawned
void AProjectile::BeginPlay()
{
	Super::BeginPlay();
	HitBox->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);
	HitBox->OnComponentBeginOverlap.AddDynamic(this, &AProjectile::OnOverlap);
	
}

void AProjectile::OnOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (class AEnemy* Enemy = Cast<AEnemy>(OtherActor)) {
		UE_LOG(LogTemp, Warning, TEXT("Projectile hit enemy"));
		DamageEnemy(Enemy);
	}
}

void AProjectile::setDamage(int towerATK)
{
	damage = towerATK;
}

void AProjectile::DamageEnemy_Implementation(AEnemy* enemyHit)
{
	UE_LOG(LogTemp, Warning, TEXT("SendingDamage"));
	enemyHit->FindComponentByClass<UHealth>()->TakeDamage(damage);
	Destroy();
}

// Called every frame
void AProjectile::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

