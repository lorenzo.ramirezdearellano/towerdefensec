// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "WaveAssest.generated.h"

/**
 * 
 */

USTRUCT(BlueprintType)
struct FSpawnDetails {

	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		TSubclassOf<class AEnemy> EnemyType;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		int SpawnPointIndex;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		float SpawnDelay;

};
UCLASS(BlueprintType)
class TOWERDEFENSEC_API UWaveAssest : public UDataAsset
{
	GENERATED_BODY()
	
public: 
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Wave Details")
	float WaveDuration;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Wave Details")
	int32 NumOfSpawns;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Wave Details")
	TArray<TSubclassOf<class AEnemy>> EnemyTypes;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Wave Details")
	TSubclassOf<class AEnemy> Boss;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "SpawnDetails");
		TArray<FSpawnDetails> EnemySpawnConfig;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "SpawnDetails");
		int DifficultyMultiplier;


	UPROPERTY(EditAnywhere, BlueprintReadWrite,Category = "Reward" )
	int KillReward;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Reward")
	int WaveReward;


};
