// Fill out your copyright notice in the Description page of Project Settings.


#include "TDGameMode.h"


void ATDGameMode::BeginPlay() {
	Super::BeginPlay();

	UGameplayStatics::GetAllActorsOfClass(GetWorld(), ASpawner::StaticClass(), Spawners);
	GetWorld()->GetTimerManager().SetTimer(WaveTimer, this, &ATDGameMode::WaveStarted, WaveInterval);
	for (AActor* actor : Spawners) {
		UE_LOG(LogTemp, Warning, TEXT("SendingWaveData"));
		savedSpawner = Cast<ASpawner>(actor);
		savedSpawner->enemySpawned.AddDynamic(this, &ATDGameMode::EnemySpawned);
	}
	player = Cast<APlayerActor>(UGameplayStatics::GetActorOfClass(GetWorld(), APlayerActor::StaticClass()));
}

void ATDGameMode::EnemyDefeated(class AEnemy* enemyDefeated, int KillReward)
{
	UE_LOG(LogTemp, Warning, TEXT("EnemyDestroyed"));
	player->AddPlayerGold(KillReward);
	NumofEnemiesDestroyed++;
	enemyDefeated->OnDeath.RemoveDynamic(this, &ATDGameMode::EnemyDefeated);
	if (NumofEnemiesDestroyed == Waves[currentWave]->NumOfSpawns) {
		WaveClear();
	}
}

void ATDGameMode::WaveStarted()
{
	NumofEnemiesDestroyed = 0;
	GetWorld()->GetTimerManager().ClearAllTimersForObject(this);
	for (AActor* actor : Spawners) {
		savedSpawner = Cast<ASpawner>(actor);
		savedSpawner->WaveStarting(Waves[currentWave]);
	}
	//waveStart.Broadcast(Waves[currentWave]);
}

void ATDGameMode::SendingWaves(ASpawner* spawner)
{
	spawner->WaveStarting(Waves[currentWave]);
}

void ATDGameMode::DamageTaken(int damage)
{
	playerHealth -= damage;
}

int ATDGameMode::GetDifficultyMultiplier()
{
	return Waves[currentWave]->DifficultyMultiplier;
}

void ATDGameMode::EnemySpawned(AEnemy* enemySpawned, ASpawner* currentSpawner) {
	savedSpawner = currentSpawner;
	UE_LOG(LogTemp, Warning, TEXT("EnemySpawned"));
	enemySpawned->OnDeath.AddDynamic(this, &ATDGameMode::EnemyDefeated);
}

void ATDGameMode::WaveClear()
{
	if (currentWave < Waves.Num()-1) {
		UE_LOG(LogTemp, Warning, TEXT("WaveClear"));
		currentWave++;
		player->AddPlayerGold(Waves[currentWave]->WaveReward);
		GetWorld()->GetTimerManager().SetTimer(WaveTimer, this, &ATDGameMode::WaveStarted, WaveInterval);
	}
	//give clear gold to player

}
