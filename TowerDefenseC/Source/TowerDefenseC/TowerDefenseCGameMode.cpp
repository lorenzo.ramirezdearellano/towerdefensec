// Copyright Epic Games, Inc. All Rights Reserved.

#include "TowerDefenseCGameMode.h"
#include "TowerDefenseCPlayerController.h"
#include "TowerDefenseCCharacter.h"
#include "UObject/ConstructorHelpers.h"

ATowerDefenseCGameMode::ATowerDefenseCGameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = ATowerDefenseCPlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/TopDownCPP/Blueprints/TopDownCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}