// Fill out your copyright notice in the Description page of Project Settings.


#include "Tower_Base.h"

// Sets default values
ATower_Base::ATower_Base()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Static Mesh"));
	ActivationRange = CreateDefaultSubobject<USphereComponent>(TEXT("Activation Range"));
	ActivationRange->SetCollisionProfileName("Trigger");
	ActivationRange->SetupAttachment(RootComponent);

}

void ATower_Base::ActivateTower_Implementation()
{
}

// Called when the game starts or when spawned
void ATower_Base::BeginPlay()
{
	Super::BeginPlay();
	ActivationRange->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);
	ActivationRange->SetSphereRadius(towerData->Stats.Range[TowerStats.Level] * 200.0);
	ActivationRange->OnComponentBeginOverlap.AddDynamic(this, &ATower_Base::OnOverlapBegin);
	ActivationRange->OnComponentEndOverlap.AddDynamic(this, &ATower_Base::OnOverlapEnd);

	TowerStats.Level = 0;
	StaticMesh->SetStaticMesh(towerData->StaticMesh);
	StaticMesh->SetMaterial(0,towerData->LevelMaterial[TowerStats.Level]);
	TowerStats.TowerCost = towerData->Stats.TowerCost[TowerStats.Level];
	SetStats();
}

void ATower_Base::acquireTarget(AActor* target)
{
	UE_LOG(LogTemp, Warning, TEXT("TargetAdded"));
	targets.Add(target);
}

void ATower_Base::removeTarget(AActor* target)
{
	UE_LOG(LogTemp, Warning, TEXT("TargetRemoved"));
	targets.Remove(target);
	ArrayItr = 0;
}

void ATower_Base::BuffingStats(int PChange, int ASChange)
{
	TowerStats.Power += PChange;
	TowerStats.FireRate += ASChange*0.5;
}

void ATower_Base::OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	UE_LOG(LogTemp, Warning, TEXT("OverlapBegan"));
	acquireTarget(OtherActor);
}

void ATower_Base::OnOverlapEnd(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	removeTarget(OtherActor);
}

void ATower_Base::RotateToTarget(AActor* target)
{
	if (WillRotate == true) {
		FRotator newRot = UKismetMathLibrary::FindLookAtRotation(this->GetActorLocation(), target->GetActorLocation());
		FRotator calculatedRot = FRotator(0.0, newRot.Yaw-90, 0.0);
		this->SetActorRotation(calculatedRot);
	}
}

void ATower_Base::Upgrade()
{
	TowerStats.Level += 1;
	StaticMesh->SetMaterial(0, towerData->LevelMaterial[TowerStats.Level]);
	SetStats();

}

void ATower_Base::SetStats()
{
	TowerStats.Power = towerData->Stats.Power[TowerStats.Level];
	TowerStats.FireRate = towerData->Stats.FireRate[TowerStats.Level];
	TowerStats.TowerCost = towerData->Stats.TowerCost[TowerStats.Level];
}

// Called every frame
void ATower_Base::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}


//acquire target

//remove target

//activate

