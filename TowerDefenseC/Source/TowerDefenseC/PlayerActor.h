// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "PlayerActor.generated.h"

UCLASS()
class TOWERDEFENSEC_API APlayerActor : public ACharacter
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	APlayerActor();

	int GetPlayerGold();
	UFUNCTION(BlueprintCallable)
	void AddPlayerGold(int goldToAdd);
	UFUNCTION(BlueprintCallable)
	void DamageTaken(int damage);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int playerGold;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int playerHealth;


public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
