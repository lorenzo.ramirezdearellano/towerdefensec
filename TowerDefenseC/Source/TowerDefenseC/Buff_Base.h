// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Buff_Base.generated.h"

UCLASS()
class TOWERDEFENSEC_API ABuff_Base : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABuff_Base();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere,BlueprintReadWrite)
		float duration;
	UFUNCTION(BlueprintCallable)
		void SetTarget(AActor* targetToSet);
	UFUNCTION(BlueprintCallable,BlueprintNativeEvent)
		void ActivateEffect();
	UFUNCTION(BlueprintCallable,BlueprintNativeEvent)
		void DeactivateEffect();
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool Islooping=false;

	FTimerHandle buffLifespan;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

private:
	UPROPERTY(EditAnywhere,BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
		class AActor* Target;

};
