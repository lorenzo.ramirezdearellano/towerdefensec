// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/SphereComponent.h"
#include "Engine/DataAsset.h"
#include "Kismet/KismetMathLibrary.h"
#include "TowerData.h"
#include "Tower_Base.generated.h"

USTRUCT(BlueprintType)
struct FTowerStats
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere,BlueprintReadOnly)
		int Power;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		float FireRate;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		float TowerCost;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		int Level;

};



UCLASS()
class TOWERDEFENSEC_API ATower_Base : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATower_Base();

	UFUNCTION(BlueprintNativeEvent,BlueprintCallable)
		void ActivateTower();


protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UFUNCTION(BlueprintCallable)
	void acquireTarget(AActor* target);
	UFUNCTION(BlueprintCallable)
	void removeTarget(AActor* target);
	UFUNCTION(BlueprintCallable)
		void BuffingStats(int PChange, int ASChange);
	UFUNCTION()
		void OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	UFUNCTION()
		void OnOverlapEnd(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);
	UFUNCTION(BlueprintCallable)
		void RotateToTarget(AActor* target);
	UFUNCTION(BlueprintCallable)
		void Upgrade();
	UFUNCTION()
		void SetStats();

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class UStaticMeshComponent* StaticMesh;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class USphereComponent* ActivationRange;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int ArrayItr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite);
		TArray<AActor*> targets;
	UPROPERTY(EditAnywhere, BlueprintReadWrite);
		UTowerData* towerData;




public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

private:
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"));
		FTowerStats TowerStats;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"));
		bool WillRotate = true;
};			
