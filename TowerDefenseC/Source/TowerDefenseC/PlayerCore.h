// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/BoxComponent.h"
#include "PlayerActor.h"
#include "Kismet/GameplayStatics.h"
#include "Enemy.h"
#include "PlayerCore.generated.h"

UCLASS()
class TOWERDEFENSEC_API APlayerCore : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	APlayerCore();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UFUNCTION()
		void OnHit(class AActor* SelfActor, class AActor* OtherActor, FVector NormalImpulse, const FHitResult& SweepResult);

	void TakingDamage(int damage);

		UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class UStaticMeshComponent* StaticMesh;

private:
public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
