// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Tower_Base.h"
#include "Build_Node.generated.h"

UCLASS()
class TOWERDEFENSEC_API ABuild_Node : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABuild_Node();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(BlueprintReadWrite)
		class ATower_Base* CurrentTower;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
