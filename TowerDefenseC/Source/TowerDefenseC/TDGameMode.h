// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "Engine/DataAsset.h"
#include "PlayerActor.h"
#include "Spawner.h"
#include "Enemy.h"
#include "WaveAssest.h"
#include "TDGameMode.generated.h"

/**
 * 
 */

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FWaveStart, UWaveAssest*, wavedata);

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FenemyDefeated, int, killReward);

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FwaveClear, int, clearReward);

UCLASS()
class TOWERDEFENSEC_API ATDGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	void BeginPlay();

	void DamageTaken(int damage);

	FWaveStart waveStart;
	FenemyDefeated enemyKilled;
	FwaveClear waveCleared;

	UFUNCTION()
		int GetDifficultyMultiplier();
	UFUNCTION()
		void EnemyDefeated(class AEnemy* enemyDefeated,int KillReward);

protected: 

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int playerHealth;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int NumofEnemiesDestroyed;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int currentWave;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float WaveInterval;


	UFUNCTION()
	void EnemySpawned(class AEnemy* enemySpawned, class ASpawner* currentSpawner);
	UFUNCTION()
	void WaveStarted();
	UFUNCTION()
	void SendingWaves(class ASpawner* spawner);
	UFUNCTION()
	void WaveClear();



	FTimerHandle WaveTimer;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TArray<UWaveAssest*> Waves;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TArray<class AActor*> Spawners;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class ASpawner* savedSpawner;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class APlayerActor* player;
	
	
};
