// Fill out your copyright notice in the Description page of Project Settings.


#include "Enemy.h"
#include "MyAIController.h"

void AMyAIController::OnMoveCompleted(FAIRequestID RequestID, const FPathFollowingResult& Result)
{

	class AEnemy* Enemy = Cast<AEnemy>(GetPawn());

	if (Enemy) {
		Enemy->SetWaypoint();
	}
}
