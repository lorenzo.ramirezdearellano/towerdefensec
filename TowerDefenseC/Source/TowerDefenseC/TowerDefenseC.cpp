// Copyright Epic Games, Inc. All Rights Reserved.

#include "TowerDefenseC.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, TowerDefenseC, "TowerDefenseC" );

DEFINE_LOG_CATEGORY(LogTowerDefenseC)
 