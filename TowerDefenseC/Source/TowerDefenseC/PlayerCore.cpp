// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerCore.h"

// Sets default values
APlayerCore::APlayerCore()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Static Mesh"));

}

// Called when the game starts or when spawned
void APlayerCore::BeginPlay()
{
	Super::BeginPlay();

	OnActorHit.AddDynamic(this, &APlayerCore::OnHit);
	
}

void APlayerCore::OnHit(AActor* SelfActor, AActor* OtherActor, FVector NormalImpulse, const FHitResult& SweepResult)
{
	if (AEnemy* Enemy = Cast<AEnemy>(OtherActor)) {
		TakingDamage(1);
		Enemy->OnDeath.Broadcast(Enemy, 0);
		Enemy->Destroy();
		UE_LOG(LogTemp, Warning, TEXT("Enemy Hit Core"));
	}
}

// Called every frame
void APlayerCore::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void APlayerCore::TakingDamage(int damage) {
	APlayerActor* player = Cast<APlayerActor>(UGameplayStatics::GetActorOfClass(GetWorld(), APlayerActor::StaticClass()));
	if (player) {
		player->DamageTaken(damage);
	}
}

