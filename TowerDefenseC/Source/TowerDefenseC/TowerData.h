// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "Materials/Material.h"
#include "TowerData.generated.h"

/**
 * 
 */
USTRUCT(BlueprintType)
struct FStatLevels {

	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		TArray<int> Power;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		TArray<int> FireRate;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		TArray<int> Range;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		TArray<int> TowerCost;
};


UCLASS(BlueprintType)
class TOWERDEFENSEC_API UTowerData : public UDataAsset
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "TowerStats")
		FStatLevels Stats;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class UStaticMesh* StaticMesh;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class USphereComponent* ActivationRange;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TArray<UMaterial*> LevelMaterial;


};
