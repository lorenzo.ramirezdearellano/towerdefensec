// Fill out your copyright notice in the Description page of Project Settings.


#include "Health.h"

// Sets default values for this component's properties
UHealth::UHealth()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UHealth::BeginPlay()
{
	Super::BeginPlay();

	// ...
	CurrentHealth = MaxHealth;
	
}

void UHealth::TakeDamage(int damage)
{
	UE_LOG(LogTemp, Warning, TEXT("TakingDamage"));
	CurrentHealth -= damage;

	if (CurrentHealth <= 0) {
		AEnemy* Owner = Cast<AEnemy>(GetOwner());
		Owner->Die();
	}
}

void UHealth::BuffHealth(float multiplier)
{
	MaxHealth *= multiplier;
}


// Called every frame
void UHealth::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

